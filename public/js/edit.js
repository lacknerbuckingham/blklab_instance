(function(){
	'use strict';

	var body = document.querySelector('body');
	var win = $(window);
	var doc = $(document.documentElement);
	var loaded = false;
	
	window.Editor = function(model){
		var _self = this;
		this.model = model;
		this.menuOpen = false;
		this.toolbar;
		this.layoutBtn;
		this.metaBtn;
		this.editmenu;
		this.content;
		this.body;
		this.buttons = {};
		this.blocks;
		this.insertionPoints;
		this.editbar;
		this.timer;
		this.editBlock;
		this.drag = 0;
		this.startDrag;
		this.linkUpdating = false;
		this.link;
		this.linkDialog;
		this.sort;
		this.htmlBlocks = ['h1','h2','h3','p', 'blockquote'];
		this.textStyles = {'bold':'b', 'italic':'i'};
		this.textAlignments = ['justifyLeft', 'justifyRight', 'justifyCenter'];
		this.currentTag;
		this.editButtons;
		this.point;

		this.init = function(){
			if(!loaded){
				_self.load();
				loaded = true;
			}
		}

		this.update = function(){
			this.enableAllTextblocks();
		}

		this.load = function(){
			this.toolbar = $('#blklab-toolbar');
			this.editmenu = $('#menu');
			this.content = $('#content');
			this.drag = $('[draggable]');
			this.body = document.querySelector('body');
			TextEditor.editbar = $('#editbar');

			
			this.sort = new Layout('blklab-block', 'content', _self, _self.toggleMenu);

			this.buttons['layout'] = $('.layoutBtn').first();
			this.buttons['meta'] = $('.metaBtn').first();
			this.buttons['save'] = $('.saveBtn').first();
			this.buttons['edit_html'] = $('.editCodeBtn').first();			


			this.buttons.layout.click(this.toggleMenu);
			this.buttons.meta.click(this.openModal);
			this.buttons.save.click(this.savePage);
			this.buttons.edit_html.click(this.editHTML);

			var editLayout = $('.editLayoutBtn').first();
			editLayout.click(function(){
				//console.log('test');
				_self.sort.toggle();
				editLayout.toggle_class('on');
				TextEditor.active = !TextEditor.active;
			})

			TextEditor.load();

			$('.block-btn').click(_self.addBlock);

			win.click(function(ev){
				var target = Evnt.target(ev);
				if(_self.menuOpen && !target.has_class('layoutBtn')){
					_self.toggleMenu(ev);
				}
			});
			
			
			this.enableBlocks();
			if(BlkLab.Browser.is_safari){
				$('.blklab-block-text').first().add_class('loaded');
			}

		}
		
		this.enableBlocks = function(){
			Layout.blocks = [];
			$('.blklab-block').each(function(el){
				_self.enableBlock(el);
			});
		},
		
		this.editHTML = function(){
			var html = _self.content.innerHTML;
			
			var texteditor = BlkLab.create('div', {'class': 'editor'});
			
			var h2 = BlkLab.create('h2', {});
			h2.innerHTML = '<span class="fa fa-code"> Edit HTML';
			
			var textarea = BlkLab.create('pre', {'id': 'html_outer_content'});
			var code = BlkLab.create('code', {'id': 'html_content'});			
			code.innerText =  _self.formatHTML(html);
			code.innerHTML = code.innerHTML.replace(/_spacer_/g, '<pre class="blklab-indent">    </pre>');
			code.contentEditable = true;
			textarea.append(code);
			
			code.on('keydown', function(ev){
				if(ev.which == '9'){
					ev.preventDefault();
					ev.stopPropagation();					
					console.log('test');
					document.execCommand('insertHTML', false, '<pre class="blklab-indent">    </pre>');
				}
				
			});
			
			var tools = BlkLab.create('div', {'class': 'tools'});
			var save = BlkLab.create('input', {'type': 'button', 'value': 'Save'});
			var cancel = BlkLab.create('input', {'type': 'button', 'value': 'Cancel'});			
			
			tools.append([save, cancel]);
			texteditor.append([h2, textarea, tools]);
			var modal = new Dialog('editor', texteditor);
			modal.add_class('open');
			modal.add_class('tall');			
			
			code.css({
				height: modal.bounds().h - 158 + 'px'
			});
			
			var doc = $(document.documentElement);
			var wrapper = $('wrapper');		

			doc.add_class('noscroll');
			wrapper.add_class('disabled');			
			
			save.click(function(ev){
				doc.remove_class('noscroll');
				wrapper.remove_class('disabled');
				
				var re = new RegExp(String.fromCharCode(160), "g");
				_self.content.innerHTML = textarea.innerText.replace('<pre class="blklab-indent">    </pre>', '').replace(re, '').replace(/\r?\n/g, "");
				_self.enableBlocks();
				BlkLab.App.lazyLoad();
				
				modal.remove_class('open');
				var timer = setInterval(function(){
					clearInterval(timer);
					timer = null;
					modal.destroy();
				},300);
			});
			
			cancel.click(function(ev){
				doc.remove_class('noscroll');
				wrapper.remove_class('disabled');				
				modal.remove_class('open');
				var timer = setInterval(function(){
					clearInterval(timer);
					timer = null;
					modal.destroy();
				},300);
			});		
		}
		
		this.formatHTML = function(html){
			//var reg = /(>)|(>)(<)(\/*)/g;
			//var reg = /([\/.*?]>)/g
			html = html.trim();
			var tmp = document.createElement('div');
			tmp.innerHTML = html;
			var newHTML = '';
			var noclose = ['input', 'source', 'img'];
			var nowrap = ['i', 'em', 'span', 'p'];			
			
			function format(tmp, ret, depth){
				var tags = tmp.childNodes;
				var i;
				var a;
				var ret = '';
				var attrs = [];
				var lastNode;
				var tagStack = [];			
				var tag;
				var wrap = '\n';
				
				var pad = '';
				var s;
				for(s=1;s<depth;s++){
					pad += '_spacer_';	
				}

				for(i=0;i<tags.length;i++){
					tag = tags[i];
					if(tag.nodeName != '#text'){
						var tagname = tag.tagName.toLowerCase();
						var attributes = tag.attributes;
						var children = tag.childNodes.length;
						var content = '';
						var c;

						for(a=0;a<attributes.length;a++){
							if(attributes[a]){
								var name = attributes[a].name || '';
								var value = attributes[a].value || '';
								attrs.push(name + '="' + value + '"');
							}
						}

						var attr = attrs.length > 0 ? ' ' + attrs.join(' ') + '' : '';				
						
						
						//if(nowrap.indexOf(tagname) == -1){
							wrap = '\n';
						//}
						
						ret += pad + '<' + tagname + attr + '>' + wrap;
						if(children > 0){
							ret += format(tag, ret, depth + 1);
						}
						if(noclose.indexOf(tagname) == -1){
							ret += pad + '</' + tagname + '>' + wrap;		
						}

						attrs = [];
					}else{
						ret += pad + tag.textContent.trim() + '\n';	
					}
				}
				
				return ret;
			}
			
			newHTML = format(tmp, '', 1);
			
			return newHTML;
		}
		
		this.savePage = function(){
			//console.log('test');
			console.log(_self);
			var d = []
			$('.blklab-block').each(function(el){
				var ele = $(el);
				var data = {
					'type': ele.data('type'),
					'html': ele.innerHTML
				}
				d.push(data);
			});

			var insertionPoints = $('.blklab-insertion-point');
			insertionPoints.removeAll();

			if(_self.sort.active)
				_self.sort.unload();
			//localStorage.setItem(location.href, $('content').innerHTML);

			var content = $('content').innerHTML;

			var tmp = BlkLab.create('div');
			tmp.innerHTML = content;

			var cleanChildren = function(children){
				var i;
				var len = children.length
				for(i=0; i<len;i++){
					var child = $(children[i]);
					try{
						child.removeAttribute('contenteditable');
						child.removeAttribute('style');
						child.removeAttribute('draggable');
						child.remove_class('placeheld');
						if(child.tagName == 'IMG'){
							child.removeAttribute('src');
							child.remove_class('fade');
						}
						if(child.childNodes.length > 0){
							cleanChildren(child.childNodes);
						}
						if(child.has_class('edit-btn')){
							child.destroy();	
						}
						
					}catch(e){}
				}
			}

			cleanChildren(tmp.childNodes);

			_self.model.set('html', tmp.innerHTML);
			_self.model.save().then(function(http){
				Alert(_self.model.get('title') + ' Has Been Saved');
			});
		}

		//Menus

		this.toggleMenu = function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			var target = Evnt.target(ev);

			if((target.id != 'menu' && !$(target.parentNode).has_class('block-btn') && target.parentNode.id != 'menu')){
				if(target.has_class('blklab-insertion-point')){
					_self.point = target;
				}
				_self.closeMenu();
			}
		}
		
		this.closeMenu = function(ev){
			_self.editmenu.toggle_class('show');
			_self.toolbar.toggle_class('on');
			_self.buttons.layout.toggle_class('on');
			_self.menuOpen = !_self.menuOpen;			
		}

		this.openModal = function(ev){
			var target = Evnt.target(ev);
			var type = target.data('type');
			_self.buttons[type].toggle_class('on');
			_self.toolbar.toggle_class('on');

			var path = location.pathname == '/' ? '/api/pages/home' : '/api/pages' + location.pathname;
			var data = BlkLab.dataStore[path];
			var template = BlkLab.templates['./public/js/templates/' + type + '.hbs'];
			var html = template(data);

			var doc = $(document.documentElement);
			var wrapper = $('wrapper');		

			doc.add_class('noscroll');
			wrapper.add_class('disabled');			
			
			var saveMeta = function(){
				console.log('saving');
			}
			
			var closeModal = function(){
				modal.remove_class('open');
				_self.buttons[type].remove_class('on');
				_self.toolbar.remove_class('on');
				
				doc.remove_class('noscroll');
				wrapper.remove_class('disabled');			
				
				var timer = setInterval(function(){
					clearInterval(timer);
					timer = null;
					modal.destroy();
				},300);
			}

			var modal = new Dialog('editor', html);
			modal.toggle_class('open');
			$('.cancel').click(closeModal);
			$('.submit').click(saveMeta);
			
			if(_self[type + 'Modal']){
				_self[type + 'Modal'].call(_self);
			}

		}

		this.metaModal = function(){
			var nav_title = $('#nav_title');
			var title = $('#title');
			var ident = $('#url');

			nav_title.typing(function(){
				title.value = nav_title.value;
				ident.value = nav_title.value.toLowerCase().trim().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
			});
		}

		//Blocks

		this.enableBlock = function(el){
			var target = el;
			var blockType = target.data('type') + 'Block';
			if(BlkLab[blockType]){
				var block = new BlkLab[blockType](target);
				block.render(_self.content);
				Layout.blocks.push(block);
			}

			return _self;
		},

		this.addBlock = function(ev){
			var target = Evnt.target(ev).parentNode;
			var blockType = target.data('type') + 'Block';
			if(BlkLab[blockType]){
				var block = new BlkLab[blockType]();
				block.render(_self.content, _self.point);
				_self.closeMenu(ev);
				var scroll = new BlkLab.Animation(600, 'window');
				scroll.run(window, 'scrollUp', window.scrollY, block.element.offsetTop, function (){});
				Layout.blocks.push(block);
				_self.sort.refresh();
			}

			return _self;
		}

		this.codeBlock = function(){
			console.log('New Code');
		}

		this.productBlock = function(){
			console.log('New Product');
		}

		this.calendarBlock = function(){
			console.log('New Calendar');
		}

		this.init();
	}

	window.Layout = function(cls, parent, editor, callback){
		var _self = this;
		this.dragSrcEl;
		this.selector = cls;
		this.parentName = parent;
		this.editor = editor;
		this.parent;
		this.elements;
		this.enabled = false;
		this.xdirection; //0 = right; 1 = left; -1 = same;
		this.ydirection; //0 = up; 1 = down; -1 = same;
		this.lastX = 0;
		this.lastY = 0;
		this.ghost;
		this.dragStart = false;
		this.lastTarget;
		this.callback = callback;
		this.active = false;
		this.movingElement;
		this.blocks = [];
		this.insertionPoints = [];

		this.init = function(){
			this.parent = $(this.parentName);
		}

		this.toggle = function(){
			if(_self.enabled){
				_self.enabled = false;
				_self.unload();
			}else{
				_self.enabled = true;
				_self.enable();
			}
		}
		
		this.refresh = function(){
			if(this.active){
				this.unload();
				this.enable();
			}
		}
	
		this.enable = function(){
			var blocks = Layout.blocks;
			_self.parent.add_class('active');
			_self.active = true;
			_self.elements = _self.parent.children_by_class(_self.selector);
			_self.dragSrcEl = null;
			
			var point;
			blocks.forEach(function(block, i){
				point = block.enable();
				if(point){
					_self.insertionPoints.push(point);
					point.click(_self.callback);
				}
			});
		}

		this.unload = function(){
			var blocks = Layout.blocks;
			_self.parent.remove_class('active');
			_self.active = false;
			_self.dragSrcEl = null;
			
			blocks.forEach(function(block, i){
				block.disable();
			});
		}

		this.init();
	}
})();
