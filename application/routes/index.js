//imports
var collections = require('./collections')
var admin = require('./admin')
var media = require('./media')
var site = require('./sites')
var users = require('./users')
var templates = require('./templates')
var def = require('./default')
var pages = require('./pages')

module.exports = {
	collections: {"path":"/api/collections", "module":collections},
	admin: {"path":"/admin", "module":admin},
	media: {"path":"/api/media", "module":media},
	sites: {"path":"/api/sites", "module":site},
	users: {"path":"/api/users", "module":users},
	templates: {"path":"/api/templates", "module":templates},
	'default': {"path":"/", "module":def},
	pages: {"path":"/api/pages", "module":pages},
}
