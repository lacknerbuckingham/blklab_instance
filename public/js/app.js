//console.clear();

var editor;
var content = $('#content');
var title = $('title');
var sitename = 'Lackner//Buckingham';
//Models
BlkLab.App.pagesModel = BlkLab.Model.extend();
BlkLab.App.pagesModel.url = '/api/pages';
BlkLab.App.pagesModel.identifier = 'url';

//Views
BlkLab.App.pagesView = BlkLab.View.extend({
	template: '{{{html}}}',

	render: function(el) {
		var self = this;
		var template = Handlebars.compile(this.template);
		var html = template(this.model.data);
		var ele = document.querySelector(el);
		ele.innerHTML = html;

		/*if(content.has_class('home')){
			content.css({
				paddingTop: Math.floor($('video').bounds().h) - 93 + 'px'
			});
		}*/

		title.innerHTML = this.model.data.title + ' | ' + sitename;
		
		var timer = window.setTimeout(function(){
			window.clearTimeout(timer);
			BlkLab.App.lazyLoad();
		}, 300);
		
	}
});

//Controllers
BlkLab.App.pagesController = BlkLab.Controller.extend({
	isEditing: false,
	navOpen: false,
	header: null,
	nav: null,

	actions: {
		open_nav: function(){
			var nav = BlkLab.App.pagesController.nav;
			var header = BlkLab.App.pagesController.header;
			if(BlkLab.App.pagesController.navOpen){
				BlkLab.App.pagesController.navOpen = false;
				nav.remove_class('open');
				header.remove_class('open');
			}else{
				BlkLab.App.pagesController.navOpen = true;
				nav.add_class('open');
				header.add_class('open');
			}
		}
	},

	render: function(route) {
		route = route != '/' ? route : '/home';
		BlkLab.App.pagesController.nav = $('nav');
		BlkLab.App.pagesController.header = $('header');
		BlkLab.App.pagesModel.find({
			id: route
		}).then(function(model) {
			var view = new BlkLab.App.pagesView();
			view.model = model;
			view.render('#content');
			BlkLab.App.pagesController.refreshActions();
			if(model.get('logged_in')){
				if(!editor){
					editor = new Editor(model);
				}				
			}
		});
	}
});

BlkLab.App.setupController = BlkLab.Controller.extend({
	steps: ['user_info', 'site_structure'],
	current_step: 0,
	actions: {
		nextStep: function(e) {
			var self = BlkLab.App.setupController;
			var pkey = self.steps[self.current_step];
			self.current_step++;
			var key = self.steps[self.current_step];
			$(pkey).hide();
			$(key).show();
		},

		previousStep: function(e) {
			var self = BlkLab.App.setupController;
			var pkey = self.steps[self.current_step];
			self.current_step--;
			var key = self.steps[self.current_step];
			$(pkey).hide();
			$(key).show();
		}
	},

	render: function(route) {}
});

/*Routes
BlkLab.App.Router.route('default', {
	controller:BlkLab.App.pagesController
});*/

BlkLab.App.Router.routes({
	'/setup': {
		controller: BlkLab.App.setupController
	},

	default: {
		controller: BlkLab.App.pagesController
	}
});

//BlkLab.History.start();
BlkLab.App.run();