var model = require('../models/pages');
var sec = require('../../library/security');
var fs = require('fs-extra');
var uuid = require('node-uuid');
var img = require('../../library/image.js');
var sys = require('sys');
var exec = require('child_process').exec;

var indexTemplate = require.resolve('../views/site/index.hbs');
var loginTemplate = require.resolve('../views/admin/login.hbs');
var pageNotFoundTemplate = require.resolve('../views/common/404.hbs');
var setupTemplate = require.resolve('../views/site/setup.hbs');
var requestTemplate = require.resolve('../views/site/request.hbs');

var render = function(req, res, admin){
    model.findMainNav().then(function(nav){
		var ident = req.params.id || 'home';
		var template = 'index';

        var data = {
            logged_in: admin,
            nav: nav,
			identifier:ident
        };
        res.render('site/' + template, data);
    });
}

var controllers = {

    home: function(req, res) {
        sec.check(req).then(
            function(){
                render(req, res, true);
            },
            function(){
                render(req, res, false);
            }
        );
    },

    login: function(req, res) {
        res.render('admin/login');
    },

    pageNotFound: function(req, res) {
        var params = {
            url: req.params.id
        };

        model.findByUrl(params).then(function(data) {
            res.status(404);
            res.render('common/404', items[0].page.data);
        });
    },

    setup: function(req, res) {
        var data = {
            title: req.params.id
        };
        res.render('site/setup', data);
    },

    upload: function(req, res) {
        res.send('nil');
    },

    single: function(req, res) {
		sec.check(req).then(
            function(){
                render(req, res, true);
            },
            function(){
                render(req, res, false);
            }
        );
    },

    req: function(req, res) {
        res.render('site/request', null);
    },

	test: function(req, res){
		res.write('Test 1');
		res.write('Test 2');
		res.write('Test 3');
		res.end();
	}
};

module.exports = controllers
