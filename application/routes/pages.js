var pages = require('../controllers/pages');
var express = require('express');
var router = express.Router();

router.route('/').get(pages.all);
router.route('/').post(pages.add);
router.route('/nav').post(pages.addNavLink);
router.route('/nav/:id').delete(pages.deleteNavLink);
router.route('/nav/sort').put(pages.updateMainNavSort);
router.route('/:id').get(pages.single);
router.route('/:id').put(pages.update);
router.route('/:id').delete(pages.del);
router.route('/links/:id').get(pages.getLinks);
router.route('/links/:id').post(pages.link);
router.route('/links/:id').delete(pages.deleteLink);

module.exports = router;
