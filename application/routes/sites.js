var site = require('../controllers/sites');
var express = require('express');
var router = express.Router();

router.route('/').get(site.all);
router.route('/').post(site.add);
router.route('/:id').get(site.single);
router.route('/:id').delete(site.del);

module.exports = router;
