BlkLab.Controller = function() {}

BlkLab.Controller.prototype = {
    actions: null,
	route:null,

    render: function() {
        console.log('base');
    },

    refreshActions: function() {
        var actions = this.actions;
        for (action in actions) {
            var avail = {
                'click': '[blklab-click="' +
                    action + '"]',
                'mouseover': '[blklab-over="' + action + '"]',
                'mouseout': '[blklab-out="' + action + '"]',
                'change': '[blklab-change="' + action + '"]',				
                'dblclick': '[blklab-dblclick="' + action + '"]',
				'typing':  '[blklab-typing="' + action + '"]',
				'dragstart':  '[blklab-dragstart="' + action + '"]',				
				'drag':  '[blklab-drag="' + action + '"]',
				'dragenter': '[blklab-dragenter="' + action + '"]',
				'dragover': '[blklab-dragover="' + action + '"]',
				'dragleave': '[blklab-dragleave="' + action + '"]',				
				'dragend': '[blklab-dragend="' + action + '"]',								
				'drop':  '[blklab-drop="' + action + '"]'
            }
            var tmp;
            for (key in avail) {
                tmp = $(avail[key]);
                if (tmp.length > 0) {
                    tmp.on(key, actions[action]);
                }
            }
        }
    }
}

BlkLab.Controller.extend = function(methods) {
    var self = function() {
        BlkLab.Controller.call(this);
    }

    self.prototype = Object.create(BlkLab.Controller.prototype);
    self.prototype.constructor = self;

    for (method in methods) {
        if (method != 'actions') {
            if (self.prototype.hasOwnProperty(method) === false) {
                self.prototype[method] = methods[method];
            }
        } else {
            var actions = methods['actions'];
        }
    }

    var obj = new self();
    obj.actions = actions;
    obj.refreshActions();
    return obj;
}
