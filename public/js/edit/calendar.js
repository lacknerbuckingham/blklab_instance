BlkLab.CalendarBlock = BlkLab.Block.extend({
	element: null,
	type: 'calendar',

	actions: {},

	create: function(){
		this.element = Coffea.create('section', {'class':'blklab-block blklab-calendar'});
		this.element.data('type', 'calendar');
	},

	render: function(container, insertionPoint, callback){
		var block = this.element;
		this.loadActions();

		if(!this.editing){
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
			}else{
				container.appendChild(block);
			}

			if(callback)
				callback.call(this);
		}
	}
});
