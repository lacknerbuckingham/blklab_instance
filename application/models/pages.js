var Model = require('blklab').Neo4jModel;
var util = require('util');
var Promise = require('promise');

var PagesModel = Model.extend({
	findAll: function(){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASPAGE]->(pages)',
				'RETURN pages'
			].join('\n')
			self.load(query, null, function(err, items) {
				resolve(items);
			});
		})
	},

	findMainNav: function(){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASPAGE]->(pages)',
				'WHERE pages.isnav=true',
				'RETURN pages',
				'ORDER BY pages.nav_order'
			].join('\n')

			self.findOne(query, {}, function(err, items) {
				if (items && items.length > 0) {
					resolve(items);
				} else {
					reject(err);
				}
			});
		})
	},

	findByUrl: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASPAGE]->(page)',
				'WHERE page.url={url}',
				'RETURN page'
			].join('\n')

			self.findOne(query, params, function(err, items) {
				if (items.length > 0) {
					resolve(items);
				} else {
					reject(err);
				}
			});
		})
	},

	createPage: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})',
				'CREATE (page:Page {props})',
				'CREATE (website)-[link:HASPAGE {url: page.url}]->(page)',
				'RETURN page'
			].join('\n')


			self.insert(query, params, function(err, result) {
				resolve(result);
			});
		});
	},

	updatePage: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;

			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASPAGE]->(page)',
				'WHERE page.url={url}',
				'SET page += {props}',
				'RETURN page'
			].join('\n')

			self.update(query, params, function(err, items) {
				resolve(items);
			});
		});
	},

	deletePage: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[rel:HASPAGE]->(page)',
				'WHERE page.url={url}',
                'OPTIONAL MATCH page-[r]-()',
				'DELETE r, rel, page'
			].join('\n')


			self.del(query, params, function(err, items) {
				resolve(items);
			});
		});
	},

	linkPages: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASPAGE]->(page),',
				'(website:Website {domain:"' + host + '"})<-[:HASPAGE]->(page2)',
				'WHERE page.url={url}',
				'AND page2.url={url2}',
				'CREATE (page)-[:LINKSTO]->(page2)',
				'RETURN page, page2'
			].join('\n');

			self.insert(query, params, function(err, result) {
				resolve(result);
			});
		});
	},

	findLinkedPages: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website { domain:"' + host + '" })-[:HASPAGE]->(page)-[:LINKSTO]->(pages)',
				'WHERE page.url={url}',
				'RETURN pages'
			].join('\n')
			self.load(query, params, function(err, items) {
				resolve(items);
			});
		})
	},

	deletePageLink: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website { domain:"' + host + '" })-[:HASPAGE]->(page)-[rel:LINKSTO]->(pages)',
				'WHERE page.url={url}',
				'AND pages.url={url2}',
				'DELETE rel'
			].join('\n')
			self.del(query, params, function(err, items) {
				if(err){
					resolve(err);
				}else{
					resolve(items);
				}
			});
		})
	}
});

var model = new PagesModel({
	db:'lb',
	collection: 'pages'
});

model.schema({
	title: {type: 'String'},
	url: {type: 'String'},
	meta: {type:'Collection'},
	blocks:{type: 'Collection'}
});

module.exports = model;
