var media = require('../controllers/media');
var express = require('express');
var router = express.Router();

router.route('/').get(media.all);
router.route('/').post(media.add);
router.route('/:id').get(media.single);
router.route('/:id').put(media.update);
router.route('/:id').delete(media.del);

module.exports = router;
