(function(history){
	'use strict';
	var editor = new Editor();
	var loader = $('loader');
	var container = $('content');

	$(document).change(function(e){
		var target = Evnt.target(e);
		console.log(target);
	});

	App = {}

	App.meta = {title:"Test"};

	App.base = function(name){
		var _self = this;
		this.name = name;
		this.url = '';
		this.data = null;
		this.callback;

		this.init = function(name){
			window.onpopstate = history.onpushstate = function(e){
				_self.fetch(location);
			}
			return this;
		};

		this.onload = function(callback){
			_self.callback = callback;
			this.fetch(location);
		};

		this.fetch = function(e){
			loader.add_class('show');
			var url = ['', 'api', name, location.pathname.replace('/', '')].join('/');
			_self.data = null;
			get(url, function(http){
				if(http.response){
					var js = JSON.parse(http.response);
					_self.data = js;
				}
				if(_self.callback){
					_self.callback.bind(_self).call(_self);
				}
			},function(err){
				console.log(err);
			});
		};

		this.get = function(key){
			return _self.data[key];
		};

		this.set = function(key, val){
			_self.data[key] = val;
		};

		this.init(name);
	};

	new App.base('pages').onload(function(self){
		var content = localStorage.getItem(location.href);
		if(content){
			container.innerHTML = localStorage.getItem(location.href);
		}else{
			container.innerHTML = '';
		}
		loader.remove_class('show');
		editor.update();
	});

})(window.history);

