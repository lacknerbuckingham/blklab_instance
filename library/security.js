var http = require('http');
var model = require('../application/models/users');
var Promise = require('promise');

exports.check = function(req){
    return new Promise(function (resolve, reject){
        sess = req.session;
        if(sess.email && sess.token){
            var params = {
                username: sess.email
            };

            model.findByUsername(params).then(function(data){
                if(data.length > 0){
                    var token = data[0].user.data.token;
                    var email = data[0].user.data.username;
                    if(token == sess.token && email  == sess.email){
                        resolve();
                    }else{
                        reject();
                    }
                }else{
                    reject();
                }

            });
        }else{
            reject();
        }
    });

}

exports.authenticate = function(req, callback){
	var params = {
		username: req.body.username
	};

	model.findByUsername(params).then(function(data){
		if(data.length > 0){
			var hash = data[0].user.data.password;
			var password = req.body.password;
			bcrypt.compare(password, hash, function(err, resp) {
				if(resp){
					sess = req.session;
					sess.email = req.body.username;
					sess.token = uuid.v1();
					var params = {
						username: req.body.username,
						props: {token: sess.token}
					};
					var ret = removePassword(data[0].user.data);
					model.updateUser(params).then(function(data){
						if(data.length > 0){
							callback.call(this, true);
						}else{
							callback.call(this, false);
						}
					});
				}else{
					callback.call(this, false);
				}
			});
		}else{
			callback.call(this, false);
		}
	});
}
