BlkLab.App = {
	run: function () {
		BlkLab.App.Router.dispatch();
	},

	serialize: function (obj) {
		var str = [];
		for (var p in obj)
			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		return str.join("&");
	},

	serializeForm: function (form) {
		var elements = form.elements;
		var len = elements.length;
		var i;
		var str = {};
		for (i = 0; i < len; i++) {
			if (elements[i].name) {
				str[elements[i].name] = elements[i].value;
			}
		}
		return JSON.stringify(str);
	},

	validateForm: function (form) {
		var elements = form.elements;
		var len = elements.length;
		var i;
		var str = {};
		var valid = true;
		for (i = 0; i < len; i++) {
			if (elements[i].name && elements[i].getAttribute('required') && elements[i].value.trim() === '') {
				valid = false;
			}
		}
		return valid;
	},

	lazyLoad: function (parent) {
		var scroll_parent = parent ? parent : $(window);
		
		function loadImage(el, fn) {
			el = $(el);
			if(el.has_class('lazy') && !el.getAttribute('src')){
				var img = new Image();
				var src = el.getAttribute('data-src');
				img.onload = function () {
					el.src = src;
					el.add_class('fade');
					//fn ? fn(img) : null;
				}
				img.src = src;
			}
		}
		
		function elementInViewport(el) {
			var height = parent ? parent.bounds().h : (window.innerHeight || document.documentElement.clientHeight);
			var left = parent ? el.offsetLeft : el.getBoundingClientRect().left;
			var top = parent ? el.offsetTop : el.getBoundingClientRect().top;			
			return (
				top >= 0 && left >= 0 && top <= height
			)
		}

		
		var images = parent ? parent.querySelectorAll('IMG') :  document.querySelectorAll('IMG');
		
		var processScroll = function () {
			var i;
			for(i=0;i<images.length;i++){
				var image = images[i];
				if (elementInViewport(image)) {
					loadImage(image, function (img) {
						console.log(img);
						images.splice(i, i);
					});
				}
			}
		};

		processScroll();
		scroll_parent.on('scroll', processScroll);
	}
}

BlkLab.App.Router = new BlkLab.Router();
