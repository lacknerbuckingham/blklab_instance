BlkLab.dataStore = {}

BlkLab.Model = function(options) {
    this.super_ = {};
    this._schema = {};
    this.data = [];
    this.url;
    this.observers = {};
    this.insert = true;
    this.primary;
	this.identifier = 'id';
}

BlkLab.Model.prototype = {
    init: function(defaults) {
		for (var key in defaults) {
            this.data[key] = defaults[key];
        }
    },

    on: function(flag, callback) {
        if (!this.observers[flag]) {
            this.observers[flag] = [];
        }

        this.observers[flag].push(callback);
    },

    dispatch: function(flag) {
        var args = arguments.splice(0, 1);
        var i;
        var callbacks = this.observers[flag];
        for (i = 0; i > callbacks.length; i++) {
            callbacks[i].call(this, args);
        }
    },

    schema: function(obj) {
        var self = this;
        var keys = Object.keys(obj);
        for (i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (self._schema.hasOwnProperty(obj[key]) === false) {
                self._schema[key] = obj[key];
                var def = obj[key].default;
                if (def) {
                    self.data[key] = self[key] = def;
                } else {
                    self.data[key] = self[key] = null;
                }
            }
        }
        return self;
    },

    get: function(key) {
        if (key in this.data) {
            return this.data[key];
        } else {
            return null;
        }
    },

    set: function(key, val) {
		this.data[key] = val;
        //this.dispatch('update', key);
        return this;
    },

    setData: function(data) {
        this.data = data;
        return this;
    },

    find: function(query) {
        var self = this;
		self.insert = false;
        var self = this;
        var id = query ? '/' + query.id : '';
        var url = this.url;
        url += id;
		url = url.replace(/\/\//g, '/');
        return get(url, query).then(function(http) {
			var ret = JSON.parse(http.response);
			if (ret.length == 1) {
                for (var key in ret[0]) {
                    self.primary = ret[0][key]['id'];
                    self.data = ret[0][key]['data'];
                }
            } else {
                self.each(ret, function(data, i) {
                    //var tmp = BlkLab.Model.extend();
                    for (var key in data) {
                        self.data[i] = data[key]['data']; //tmp.setData(data[key]['data']);
                    }
                });
            }
			BlkLab.dataStore[url] = self.data;
        }).then(function() {
            return new Promise(function(resolve, reject) {
                resolve(self);
            })
        })
    },

    length: function() {
        return this.data.length;
    },

    eachKey: function(callback) {
        var keys = Object.keys(this.data);
        var len = keys.length;
        var i;
        for (i = 0; i < len; i++) {
            var key = keys[i];
            var val = this.data[key];
            callback.call(this, key, val);
        }
    },

    each: function(data, callback) {
        var len = data.length;
        if (len > 1) {
            var i;
            for (i = 0; i < len; i++) {
                callback.call(self, data[i], i);
            }
        } else {
            callback.call(self, data);
        }
    },

    save: function(isnew) {
        var self = this;
		console.log(this.identifier);
		var url = isnew ? this.url : this.url + '/' + this.data[this.identifier];
		delete this.data.logged_in;
		var payload = JSON.stringify(this.data);
		if(!isnew){
			return put({
				url: url,
				data: payload,
				dataType: 'application/json'
			})
		}else{
			return post({
				url: url,
				data: payload,
				dataType: 'application/json'
			})
		}
    },

    update: function(query) {
        var self = this;
        return new Promise(function(resolve, reject) {
            resolve(self);
        })
    },

    del: function(id) {
        var self = this;
		if(id){
			var url = this.url + '/' + id;
			return del({
				url: url,
				dataType: 'application/json'
			})
		}
    }
}

BlkLab.Model.extend = function(methods, options) {
    if (arguments.length == 1) {
        options = methods;
    }
    var self = function(options) {
        BlkLab.Model.call(this, options);
    }

    self.prototype = Object.create(BlkLab.Model.prototype);
    self.prototype.constructor = self;

    if (arguments.length > 1) {
        for (method in methods) {
            if (self.prototype.hasOwnProperty(method) === false) {
                self.prototype[method] = methods[method];
            }
        }
    }

    var obj = new self(options);
    obj.init();
    return obj;
}
