var Model = require('blklab').Model;
var util = require('util');

var DefaultModel = Model.extend({});

var model = new DefaultModel({
	db:'lb',
	collection: 'default'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
