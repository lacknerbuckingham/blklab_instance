(function() {
	var form = $('form');
	var error = $('error');
	var timer;

	function showError(msg){
		error.innerHTML = msg;
		error.add_class('on');
		timer = window.setInterval(function(){
			window.clearInterval(timer);
			error.remove_class('on');
		}, 5000)
	}

    BlkLab.App.loginController = BlkLab.Controller.extend({
        actions: {
            submit: function(e) {
				var valid = BlkLab.App.validateForm(form);
				if(valid){
					var url = '/api/users/authenticate';
					var data = BlkLab.App.serializeForm(form);
					post({
						url: url,
						data: data,
						dataType: 'application/json',
						success: function(resp){
							location.href = '/admin';
						},
						fail: function(resp){
							showError('There has been an error logging you in. Either your username or password are incorrect.');
						}
					})
				}else{
					showError('Please fill out all of the required fields.');
				}
            }
        },

        render: function(route) {
			var self = this;
			window.onkeyup = function(e){
				if(e.which === 13){
					window.clearInterval(timer);
					var valid = BlkLab.App.validateForm(form);
					if(valid){
						self.actions.submit(e);
					}else{
						showError('Please fill out all of the required fields.');
					}
				}
			}
		}
    });

    BlkLab.App.Router.routes({
		default: {
            controller: BlkLab.App.loginController
        }
    });

    BlkLab.App.run();

})();
