var lib;

BlkLab.GalleryBlock = BlkLab.Block.extend({
	element: null,
	type: 'image',

	actions: {
		drop: function(ev){
			ev.stopPropagation();
			ev.preventDefault();			
			var target = Evnt.target(ev);
			var file_list = ev.dataTransfer.files;
			var meta;
			var file = file_list[0];
			
			if(file_list.length > 0){
				var block = $(this);
				while(block && !block.has_class('blklab-image')){
					block = $(block.parentNode);
				}
				this.destroy();
				
				var test = BlkLab.create('div', {'class':'placeholder', 'id': 'row_' + file.name});
				test.innerHTML = '<div class="loader loader-quart show"></div>';
				block.innerHTML = '';
				block.prepend(test);

				meta = {name: file.name, placeholder: test, callback:function(file, meta){
					if(file){
						var timer;
						var img = new Image();
						function loadImg(){
							img.src = meta.uri+'/'+meta.filename+'-lg'+meta.extension;
							img.className = 'lazy fade';

							img.onload = function(){
								try{
									file.placeholder.replaceWith(img);
								}catch(e){
									console.log(e);
								}
								window.clearInterval(timer);
							}
							var im = $(img);

							im.data('src', img.src);
							im.click(function(el){
								var img = Evnt.target(el);
								var tmp = new Image();
								tmp.src = img.data('url');
								tmp.onload = function(){
									p.src = tmp.src;
								}
							});
						}

						img.onerror = function(){
							timer = window.setInterval(function(){
								loadImg();
							}, 600);
						}

						loadImg();
					}else{
						test.destroy();
					}
				}};
				BlkLab.Frontend.Uploader.add_to_queue(file, meta);

				BlkLab.Frontend.Uploader.start();
			}
		},

		dragleave: function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			var file_list = ev.dataTransfer.files;
			var meta;
			var file = file_list[0];
			this.remove_class('entered');
		},

		dragenter: function(ev){
			ev.stopPropagation();
			ev.preventDefault();			
			var file_list = ev.dataTransfer.files;
			var meta;
			var file = file_list[0];
		},

		dragover: function(ev){
			ev.stopPropagation();
			ev.preventDefault();			
			var file_list = ev.dataTransfer.files;
			var meta;
			var file = file_list[0];
			ev.dataTransfer.dropEffect = 'move';
			this.remove_class('fade');
			this.add_class('entered');
		}
	},

	create: function(){
		this.element = Coffea.create('section', {'class':'blklab-block blklab-gallery'});
		this.element.data('type', 'Gallery');
	},

	edit: function(){		
		var element = this.element;
		var images = this.element.children_by_tag('img');
		var preview;
		var src;
		
		var data = [];
		images.each(function(img){
			if(img){
				var ext = img.src.split('.').pop();
				var base = img.src.replace('http://', '').split('/')[0];
				preview = img.src.replace(new RegExp('-lg|small|medium|thumb\.' + ext), '-thumb');
				src = img.src;
			}
			var tmp = {
				src: src,
				preview: preview
			};
			data.push(tmp);
		});
		var template = BlkLab.templates['./public/js/templates/edit_gallery.hbs'];
		var html = template({'data': data});
		
		var doc = $(document.documentElement);
		var wrapper = $('wrapper');		
		
		doc.add_class('noscroll');
		wrapper.add_class('disabled');
		
		var modal = new Dialog('editor', html);
		
		var p = $('preview');		
		var closeModal = function(){
			doc.remove_class('noscroll');
			wrapper.remove_class('disabled');
			
			modal.add_class('out');
			var timer = setInterval(function(){
				clearInterval(timer);
				timer = null;
				modal.destroy();
				wrapper = null;
				doc = null;
			},1000);
		}

		var saveImage = function(){
			element.innerHTML = '';
			element.add_class('gallery');
			var gallery = $('#blklab-gallery-preview');
			var inner = BlkLab.create('div', {'class':'slider'});
			var images = gallery.children_by_tag('IMG');
			var src;
			var nimg;
			images.each(function(img){
				nimg = new Image();
				var src = img.src.replace('-thumb', '-lg');
				nimg.src = src;
				nimg.className = 'cell';
				nimg.setAttribute('data-src', src);
				nimg.width = element.bounds().w;
				inner.append(nimg);
			});
			
			element.append(inner)
			new Gallery(inner, 'slide', images.length, element.bounds(), 0);
		}

		var removeImage = function(){
			$(element).destroy();
			closeModal();
		}
		
		$('.cancel').click(closeModal);
		$('.submit').click(saveImage);
		$('.remove').click(removeImage);
		modal.add_class('fade');		
		
		lib = $('#file_library');
		
		function uploadFiles(file_list){
			var len = file_list.length;
			var i;
			var meta;
			var file;
			
			for(i=0;i<len;i++){
				file = file_list[i];
				var test = BlkLab.create('div', {'class':'placeholder', 'id': 'row_' + file.name});
				test.innerHTML = '<div class="loader loader-quart show"></div>';
				lib.prepend(test);
				
				meta = {name: file.name, placeholder: test, callback:function(file, meta){
					if(file){
						var timer;
						var img = new Image();
						function loadImg(){
							img.src = meta.uri+'/'+meta.filename+'-thumb'+meta.extension;
							img.className = 'lazy toolbar images fade';
							
							img.onload = function(){
								try{
									file.placeholder.replaceWith(img);
								}catch(e){
									console.log(e);
								}
								window.clearInterval(timer);							
							}
							var im = $(img);
							
							im.data('url', img.src);
							im.click(function(el){
								var img = Evnt.target(el);
								var tmp = new Image();
								tmp.src = img.data('url');
								tmp.onload = function(){
									p.src = tmp.src;
								}
							});
						}
						
						img.onerror = function(){
							timer = window.setInterval(function(){
								loadImg();
							}, 600);
						}
						
						loadImg();
					}else{
						test.destroy();	
					}
				}};
				BlkLab.Frontend.Uploader.add_to_queue(file, meta);
			}
			
			BlkLab.Frontend.Uploader.start();
		}	
		
		$('#new_file').on('change', function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			var file_list = ev.target.files;
			uploadFiles(file_list);
		});
		
		lib.drop(function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			var file_list = ev.dataTransfer.files;
			uploadFiles(file_list);
		});

		lib.dragover(function(ev){
			ev.stopPropagation();
			ev.preventDefault();
		});

		lib.dragenter(function(ev){
			ev.stopPropagation();
			ev.preventDefault();
		});
		
		var tileView = $('#tile_view');
		var listView = $('#list_view');
		var self = this;
		
		tileView.click(function(ev){
			self.loadTileView();
			tileView.add_class('selected');
			listView.remove_class('selected');			
		});
		
		listView.click(function(ev){
			self.loadListView();
			tileView.remove_class('selected');
			listView.add_class('selected');						
		});
		this.loadTileView();			
	},
	
	loadListView: function(){
		var gallery = $('#blklab-gallery-preview');
		BlkLab.App.MediaModel.find().then(function(model) {
			var view = new BlkLab.App.MediaListView();
			view.model = model;
			view.render('#file_library');
			$('.images').click(function(el){
				var img = Evnt.target(el);
				var tmp = $(new Image());
				tmp.src = img.data('url');
				tmp.onload = function(){
					gallery.append(tmp);
					tmp.click(function(){
						tmp.destroy();
					});
				}
			});				
		});
	},
	
	loadTileView: function(){
		var gallery = $('#blklab-gallery-preview');
		BlkLab.App.MediaModel.find().then(function(model) {
			var view = new BlkLab.App.MediaView();
			view.model = model;
			view.render('#file_library');
			
			BlkLab.App.lazyLoad($('#file_library'));
			
			$('.images').click(function(el){
				var img = Evnt.target(el);
				var tmp = $(new Image());
				tmp.src = img.data('url');
				tmp.onload = function(){
					gallery.append(tmp);
					tmp.click(function(){
						tmp.destroy();
					});
				}
			});
		});
	},
	
	render: function(container, insertionPoint, callback){
		var block = this.element;
		var self = this;
		
		block.children_by_class('edit-btn').removeAll();
		
		var editButton = Coffea.create('div', {'class':'fa fa-info-circle edit-btn'});
		block.append(editButton);
		
		editButton.click(function(ev){
			self.edit();
		});
		
		if(!this.editing){
			var addImgButton = Coffea.create('div', {'title':'picture', 'class':'entypo blklab-new-gallery'});
			var addImgForm = Coffea.create('form', {'method':'post'});
			addImgButton.innerHTML = '🌄';
			this.loadActions(addImgButton);
			
			addImgForm.append(addImgButton);
			block.append(addImgForm);
						
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
				insertionPoint = null;
			}else{
				container.appendChild(block);
			}
			
			
			addImgButton.click(function(ev){
				self.edit();
			})

			if(callback)
				callback.call(this);
		}else{
			var inner = block.children_by_class('slider').first();
			inner.css({
				left:'0'
			})
			if(inner){
				var images = inner.children_by_tag('img');
				images.each(function(img){
					img.src = img.data('src');
					img.onload = function(){
						img.add_class('fade');	
					}
				});
				new Gallery(inner, 'slide', 0, block.bounds(), 0);			
			}
			this.loadActions();
		}
		
		
	}
});
