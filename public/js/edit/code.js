BlkLab.CodeBlock = BlkLab.Block.extend({
	element: null,
	type: 'code',

	actions: {},

	create: function(){
		this.element = Coffea.create('section', {'class':'blklab-block blklab-code'});
		this.element.data('type', 'code');
	},

	render: function(container, insertionPoint, callback){
		var block = this.element;
		this.loadActions();

		if(!this.editing){
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
			}else{
				container.appendChild(block);
			}

			if(callback)
				callback.call(this);
		}
	}
});
