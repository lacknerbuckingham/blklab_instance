var def = require('../controllers/default');
var express = require('express');
var router = express.Router();

router.route('/').get(def.home);
router.route('/test').get(def.test);
router.route('/login').get(def.login);
router.route('/page-not-found').get(def.pageNotFound);
router.route('/setup').get(def.setup);
router.route('/req').get(def.req);
router.route('/upload').post(def.upload);
router.route('/:id').get(def.single);

module.exports = router;
