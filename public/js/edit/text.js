var placeholder = '<span>Write Here...</span>';
var body = document.querySelector('body');
var win = $(window);
var doc = $(document.documentElement);
window.Clicked = false;

BlkLab.TextBlock = BlkLab.Block.extend({
	element: null,
	inner: null,
	type: 'text',
	drag: 0,
	startDrag:null,
	linkUpdating: false,
	link:null,
	linkDialog: null,
	sort: null,
	htags: ['H1','H2','H3'],
	htmlBlocks: ['h1','h2','h3','p', 'blockquote'],
	textStyles: {'bold':'b', 'italic':'i'},
	textAlignments: ['justifyLeft', 'justifyRight', 'justifyCenter'],
	currentTag: null,
	editBlock: null,

	actions: {
		mousedown: function(ev){
			TextEditor.startDrag = null;
			TextEditor.drag = 0;
		},

		mousemove: function(){
			TextEditor.startDrag = this;
			TextEditor.drag = 1;
		},

		keydown: function(ev){
			var block = this;
			var node = Ranger.getSelectionStart();
			var htags = ['H1','H2','H3'];
			
			if(ev.which === 13 && htags.indexOf(node.tagName) != -1){
				//ev.preventDefault();
				//document.execCommand('insertHTML', false, '<p></p>');
				window.Clicked = true;
			}
			
			if(block.innerHTML == this.placeholder){
				block.toggle_class('placeheld');
				block.innerHTML = '';
			}

			if(ev.which === 13){
				if(node.tagName != 'li'){
					if(!ev.shiftKey){
						//ev.preventDefault();
					}
				}
			}else{
				Ranger.save(block);
				if(ev.which >= 37 && ev.which <= 40){
					if(Ranger.selection.text != '' && block.innerHTML != ''){
						TextEditor.showToolbar();
					}else{
						TextEditor.hideToolbar();
					}
				}
			}
		},

		focus: function(ev){
			var block = this;
			var changed = false;
			if(TextEditor.editBlock != block){
				TextEditor.editBlock = block;
				changed = true;
			}

			if(block.innerHTML == placeholder){
				block.toggle_class('placeheld');
				block.innerHTML = '';
				block.focus();
			}else{
				if(!changed)
					Ranger.restore(block);
			}
		},

		blur: function(ev){
			var block = this;

			if(block.innerText == ''){
				block.toggle_class('placeheld');
				block.innerHTML = placeholder;
			}
			TextEditor.hideToolbar();
		},

		paste: function(e) {
			var block = this;
			e.stopPropagation();
			e.preventDefault();
			var type = TextEditor.findType(e);
			var data = e.clipboardData.getData(type);
			TextEditor.processpaste(block, data, type);
			return false;
		},

		mouseup: function(ev){
			var block = this;
			if(TextEditor.drag == 1){
				Ranger.save(block);
				if(Ranger.selection.text != ''){
					TextEditor.link.value = '';
					TextEditor.linkDialog.remove_class('on');
					TextEditor.editbar.remove_class('expanded');
					TextEditor.showToolbar();
				}else{
					TextEditor.hideToolbar();
				}
			}else{
				TextEditor.hideToolbar();
			}
		},

		keyup: function(ev){
			var block = this;

			var node = Ranger.getSelectionStart();
			
			if(ev.which >= 37 && ev.which <= 40){
				Ranger.save(block);
				if(Ranger.selection.text != '' && block.innerHTML != ''){
					TextEditor.showToolbar();
				}else{
					TextEditor.hideToolbar();
				}
			}else if(block.innerText == ''){
				block.toggle_class('placeheld');
				block.innerHTML = placeholder;
			}
		}
	},

	create: function(parent){
		this.inner = Coffea.create('section', {'class':'blklab-block blklab-text'});
		this.element = Coffea.create('section', {'class':'inner placeheld'});
		this.inner.data('type', 'Text');
		this.element.contentEditable = true;
		this.element.innerHTML = placeholder;
		this.inner.append(this.element);
	},
	
	edit: function(){
		var element = this.element;
		var data = {};
		var template = BlkLab.templates['./public/js/templates/edit_text.hbs'];
		var html = template(data);

		var doc = $(document.documentElement);
		var wrapper = $('wrapper');		
		
		doc.add_class('noscroll');
		wrapper.add_class('disabled');
		
		var closeModal = function(){
			modal.remove_class('open');
			var timer = setInterval(function(){
				doc.remove_class('noscroll');
				wrapper.remove_class('disabled');
				clearInterval(timer);
				timer = null;
				modal.destroy();
			},300);
		}

		var save = function(){}

		var remove = function(){
			$(element).destroy();
			closeModal();
		}
		
		

		var modal = new Dialog('editor', html);
		modal.toggle_class('open');
		$('.cancel').click(closeModal);
		$('.submit').click(save);
		$('.remove').click(remove);
	},

	render: function(container, insertionPoint, callback){
		if(!this.editing){
			var block = this.inner;
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
				insertionPoint = null;
			}else{
				container.appendChild(block);
			}

			if(callback)
				callback.call(this);
		}else{
			this.inner = this.element;
			this.element = this.inner.children_by_class('inner').first();
			this.element.contentEditable = "true";
			this.element.setAttribute('contenteditable', "true");
		}
		
		this.element.addEventListener("DOMNodeInserted", function(ev){
			try{
				ev.target.removeAttribute('style');
				ev.target.removeAttribute('class');			
			}catch(e){}
			
			var cleanup = function(){
				var range    = document.createRange();
				var sel      = window.getSelection();
		        var pNode    = document.createElement('p');
				ev.target.parentNode.replaceChild(pNode, ev.target);
				
				var placeCursor = function () {
					pNode.innerHTML = "<br>";
					range.setStart(pNode, 0);
					sel.removeAllRanges();
					sel.addRange(range);    
					//pNode.innerHTML = '';
				}
				
				setTimeout(placeCursor, 50);	
			}
			
			if(!Coffea.Browser.is_mozilla){
				if((ev.target.nodeName == 'DIV' && (ev.target.innerHTML == '<br>' || ev.target.innerHTML == ''))){
					console.log('div');
					cleanup();	
				}

				if(TextEditor.htags.indexOf(ev.target.nodeName) != -1 && ev.target.innerHTML == ''){
					console.log('h tag');				
					cleanup();
				}
			}
			
			/*if(TextEditor.htags.indexOf(ev.target.nodeName) != -1 && window.Clicked){
				ev.target.parentNode.removeChild(ev.target);	
			}
			
			if(ev.target.nodeName == 'P' && ev.target.innerHTML == ''){
				var range    = document.createRange();
				var sel      = window.getSelection();	
				ev.target.innerHTML = 'Text';
				range.setStart(ev.target, 0);
				sel.removeAllRanges();
				sel.addRange(range); 
				ev.target.innerHTML = '';				
			}
			
			window.Clicked = false;*/
		}, false);
		this.loadActions();
	},

	init: function(){}
});

window.TextEditor = {
	stopping: false,
	drag: 0,
	startDrag:null,
	linkUpdating: false,
	link:null,
	linkDialog: null,
	sort: null,
	htags: ['H1','H2','H3'],	
	htmlBlocks: ['h1','h2','h3','p', 'blockquote'],
	textStyles: {'bold':'b', 'italic':'i'},
	textAlignments: ['justifyLeft', 'justifyRight', 'justifyCenter'],
	currentTag: null,
	editBlock: null,
	active:true,
	editbar: null,
	editButtons:null,
	timer:null,

	load: function(){
		var _self = TextEditor;
		var linkDialog = TextEditor.linkDialog = $('link_dialog');
		var link = TextEditor.link = $('link_address');
		var linkAdd = $('link_add');

		var addLink = function(){
			var href = link.value;
			if(!TextEditor.linkUpdating){
				var selection = Ranger.restore(_self.editBlock);
				var node = Ranger.getSelectionNode();
				var link_text = Ranger.selection.text;
				var html = '<a href="' + href + '">' + link_text + '</a>';
				document.execCommand('insertHTML', false, html);
			}else{
				if(link.value != ''){
					Ranger.selection.anchor.parentElement.href = link.value;
				}else{
					var node = document.createTextNode(Ranger.selection.anchor.data);
					Ranger.selection.anchor.parentElement.parentElement.replaceChild(node, Ranger.selection.anchor.parentElement);
				}
			}

			linkDialog.toggle_class('on');
			_self.editbar.toggle_class('expanded');
			_self.hideToolbar();
			link.value = '';
			_self.linkUpdating = false;
		}

		doc.on('keydown', function(ev){
			if(ev.which === 13){
				if(Evnt.target(ev) == link){
					ev.preventDefault();
					addLink();
				}
			}
		});

		linkAdd.click(addLink);

		this.editButtons = TextEditor.editButtons = $('.blklab-editbar-button');

		TextEditor.editButtons.click(TextEditor.editButton);

		doc.doubleclick(function(ev){
			var tmp = Ranger.selection;
			Ranger.save(Evnt.target(ev));
		});

		doc.on('mousedown', function(){
			_self.startDrag = null;
			_self.drag = 0;
		});

		doc.on('mouseup', function(){
			if(_self.startDrag && _self.drag){
				window.clearInterval(_self.timer);
				_self.stopping = false;
				_self.link.value = '';
				_self.linkDialog.remove_class('on');
				_self.editbar.remove_class('expanded');
				_self.showToolbar();
			}
		});
	},

	showToolbar: function() {
		if(this.active){
			var toolbar = this.editbar;
			var defaultLeft = 0;
			var buttonHeight = 100,
				selection = window.getSelection(),
				range = selection.getRangeAt(0),
				boundary = range.getBoundingClientRect(),
				defaultLeft = (toolbar.offsetWidth / 2),
				middleBoundary = (boundary.left + boundary.right) / 2,
				halfOffsetWidth = toolbar.offsetWidth / 2;
			if(boundary.top < buttonHeight) {
				toolbar.style.top = (buttonHeight + boundary.bottom + window.pageYOffset - toolbar.offsetHeight) + 'px';
			}else{
				toolbar.style.top = (boundary.top + window.pageYOffset - toolbar.offsetHeight) + 'px';
			}
			if(middleBoundary < halfOffsetWidth){
				toolbar.style.left = halfOffsetWidth + 'px';
			} else if ((window.innerWidth - middleBoundary) < halfOffsetWidth) {
				toolbar.style.left = window.innerWidth + defaultLeft - halfOffsetWidth + 'px';
			} else {
				toolbar.style.left = middleBoundary - halfOffsetWidth + 'px';
			}

			this.currentTag = Ranger.getSelectionStartNode();

			this.activateButtons();

			this.editbar.add_class('show');
		}
		return this;
	},

	activateButtons: function(){
		var self = this;
		this.editButtons.each(function(el){
			if(self.currentTag.tagName.toLowerCase() == el.data('tag')){
				el.add_class('on');
				if(self.currentTag.tagName.toLowerCase() == 'a'){
					self.link.value = self.currentTag.href;
				}
			}else if(self.currentTag.className && self.currentTag.className.indexOf(el.data('type')) != -1){
				el.add_class('on');
				if(self.currentTag.tagName.toLowerCase() == 'a'){
					self.link.value = self.currentTag.href;
				}
			}else{
				el.remove_class('on');
			}
		});
	},

	hideToolbar: function(){
		if(this.editbar.has_class('show') && !this.stopping){
			this.stopping = true;
			this.timer = window.setInterval(function(){
				window.clearInterval(this.timer);
				this.editbar.remove_class('show');
				this.stopping = false;
			}, 500);
		}else{
			window.clearInterval(this.timer);
			this.stopping = false;
		}
	},

	findType: function(e){
		if (/text\/html/.test(e.clipboardData.types)) {
			return 'text/html';
		} else if (/text\/plain/.test(e.clipboardData.types)) {
			return 'text/plain';
		} else if (/text\/rtf/.test(e.clipboardData.types)) {
			return 'text/rtf';
		}else{
			return 'text/html';
		}
	},

	processpaste: function(elem, savedcontent, type) {
		document.execCommand('insertHTML', false, this.cleanHTML(savedcontent));
	},

	cleanHTML: function(html, exclude){
		var tmp = Coffea.create('div',{});
		var re = new RegExp("\u00a0", "g");
		tmp.innerHTML = html.trim().replace(/<!--[\s\S]*?-->/g, '').replace(re, " ");
		this.cleanChildren(tmp.childNodes, exclude);
		var ret = tmp.innerHTML.replace(/<br>/g, '').replace(/&nbsp;/g, ' ');
		var len = tmp.children_by_tag('*').length;
		if(len <= 1){
			ret = '<p>' + ret + '</p>';
		}
		tmp.destroy();
		return ret;
	},

	cleanChildren: function(tmp, exclude){
		var ch = tmp;
		var arr = []
		var self = this;
		for (var i = 0, ref = arr.length = ch.length; i < ref; i++){arr[i] = ch[i];}
		var i;
		for(i=0; i<arr.length; i++){
			var el = arr[i];
			if(el.tagName){
				if(el.tagName.toLowerCase().trim() == 'meta' || el.tagName.toLowerCase().trim() == 'link' || el.tagName.toLowerCase().trim() == 'o:p'){
					el.parentNode.removeChild(el);
				}else if(el.innerHTML == ''){
					if(el.tagName.toLowerCase().trim() != 'br' && el.tagName.toLowerCase().trim() != 'img'){
						el.parentNode.removeChild(el);
					}else{
						el.removeAttribute('style');
						if(exclude != 'class'){
							el.removeAttribute('class');
						}
						el.removeAttribute('id');
					}
				}else{
					try{
						el.removeAttribute('style');
						if(exclude != 'class'){
							el.removeAttribute('class');
						}
						el.removeAttribute('id');
					}catch(e){}
					if(el.hasChildNodes()){
						self.cleanChildren(el.childNodes);
					}
				}
			}
		}
	},

	editButton: function(e){
		var _self = TextEditor;
		window.clearInterval(_self.timer);
		_self.stopping = false;
		e.preventDefault();
		e.stopPropagation();
		var ele = Evnt.target(e);
		var action = ele.data('action');

		switch(action){
			case 'format':
				var type = ele.data('type');
				if(_self.htmlBlocks.indexOf(type) != -1){
					type = type == _self.currentTag.tagName.toLowerCase().trim() ? 'p' : type;
					Ranger.restore(_self.editBlock, 'block');
					document.execCommand("formatBlock", false, type);
				}else{
					Ranger.restore(_self.editBlock);
					if(type.indexOf('justify') == -1){
						document.execCommand(type, false, true);
						if(type == 'insertUnorderedList' && _self.currentTag.tagName.toLowerCase().trim() == 'li'){
							document.execCommand("formatBlock", false, 'p');
						}
					}else{
						var i;
						for(i=0;i<_self.textAlignments.length;i++){
							_self.currentTag.remove_class(_self.textAlignments[i]);
						}
						_self.currentTag.add_class(type);
					}
				}

				Ranger.save(_self.editBlock);
				_self.currentTag = Ranger.getSelectionStartNode();
				_self.activateButtons();
				if(_self.currentTag.tagName.toLowerCase() == 'span'){
					_self.currentTag.parentNode.innerHTML = _self.cleanHTML(_self.currentTag.parentNode.innerHTML, 'class');
					_self.currentTag.parentNode.replaceChild(document.createTextNode(_self.currentTag.innerText), _self.currentTag);
				}

				break;
			case 'link_dialog':
				linkDialog.toggle_class('on');
				_self.editbar.toggle_class('expanded');
				link.focus(function(){
					window.clearInterval(_self.timer);
					_self.stopping = false;
				});
				break;
		}

	}
}

window.Ranger = {
	selection: null,
	range: null,
	parentElements: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre'],

	save: function(containerEl) {
		try{
			var doc = containerEl.ownerDocument, win = doc.defaultView;
			var range = win.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(containerEl);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;

			var i,
				len,
				ranges,
				sel = window.getSelection();
			if(sel.getRangeAt && sel.rangeCount) {
				ranges = [];
				for (i = 0, len = sel.rangeCount; i < len; i += 1) {
					ranges.push(sel.getRangeAt(i));
				}
			}

			this.selection = {
				start: start,
				end: start + range.toString().length,
				text: range.toString(),
				anchor: win.getSelection().anchorNode,
				ranges: ranges
			}

		}catch(e){
			this.selection = {
				start: 0,
				end: 0,
				text: null,
				anchor: null,
				ranges: null
			}
		}
	},

	restore: function(containerEl, type) {
		if(type == 'block'){
			var doc = containerEl.ownerDocument, win = doc.defaultView;
			var charIndex = 0, range = doc.createRange();
			range.setStart(containerEl, 0);
			range.collapse(true);
			var nodeStack = [containerEl], node, foundStart = false, stop = false;

			while (!stop && (node = nodeStack.pop())) {
				if (node.nodeType == 3) {
					var nextCharIndex = charIndex + node.length;
					if (!foundStart && this.selection.start >= charIndex && this.selection.start <= nextCharIndex) {
						range.setStart(node, this.selection.start - charIndex);
						foundStart = true;
					}
					if (foundStart && this.selection.end >= charIndex && this.selection.end <= nextCharIndex) {
						range.setEnd(node, this.selection.end - charIndex);
						stop = true;
					}
					charIndex = nextCharIndex;
				} else {
					var i = node.childNodes.length;
					while (i--) {
						nodeStack.push(node.childNodes[i]);
					}
				}
			}
			var sel = win.getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
		}else{
			var i,
				len,
				sel = window.getSelection();
			if(this.selection && this.selection.ranges){
				sel.removeAllRanges();
				for (i = 0, len = this.selection.ranges.length; i < len; i += 1) {
					sel.addRange(this.selection.ranges[i]);
				}
			}
		}

		return sel;
	},

	getSelectionStart: function (){
		var node = document.getSelection().anchorNode,
			startNode = (node && node.nodeType === 3 ? node.parentNode : node);
		return startNode;
	},

	getSelection: function() {
		return window.getSelection();
	},

	getSelectionStartNode: function() {
		var node, selection;
		if (window.getSelection) {
			// FF3.6, Safari4, Chrome5 (DOM Standards)
			selection = getSelection();
			node = selection.anchorNode;
		} else if (!node && document.selection) {
			// IE
			selection = document.selection
			var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
			node = range.commonAncestorContainer ? range.commonAncestorContainer : range.parentElement ? range.parentElement() : range.item(0);
		}

		if (node != undefined) {
			var n = (node && node.nodeType === 3 ? node.parentNode : node);
			return n;
		}
	},

	getSelectionHtml: function(){
		var i,
			html = '',
			sel,
			len,
			container;
		if (window.getSelection !== undefined) {
			sel = window.getSelection();
			if (sel.rangeCount) {
				container = document.createElement('div');
				for (i = 0, len = sel.rangeCount; i < len; i += 1) {
					container.appendChild(sel.getRangeAt(i).cloneContents());
				}
				html = container.innerHTML;
			}
		} else if (document.selection !== undefined) {
			if (document.selection.type === 'Text') {
				html = document.selection.createRange().htmlText;
			}
		}
		return html;
	},

	getSelectionNode: function(){
		var tagName;
		var el = this.selection.anchor;

		if (el && el.tagName) {
			tagName = el.tagName.toLowerCase();
		}
		if(el){
			while (el && this.parentElements.indexOf(tagName) === -1) {
				el = el.parentNode;
				if (el && el.tagName) {
					tagName = el.tagName.toLowerCase();
				}
			}

			return {
				el: el,
				tagName: tagName
			};
		}else{
			return {
				el: el,
				tagName: 'p'
			};
		}
	}
};
