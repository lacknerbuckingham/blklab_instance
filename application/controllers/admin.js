var adminModel = require('../models/admin');
var sec = require('../../library/security');
var sess;

var controllers = {
	single: function(req, res) {
        sec.check(req).then(
            function(){
                res.render('admin/dashboard', null);
            },
            function(){
                res.redirect('/login');
            }
        );
    },

	login: function(req, res) {
        res.render('admin/login');
    },

	logout: function(req, res){
		sess = req.session;
		sess.email = '';
		sess.token = '';
		res.redirect('/admin/login');
	}
};

module.exports = controllers
