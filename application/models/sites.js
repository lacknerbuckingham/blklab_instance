var Model = require('blklab').Neo4jModel;
var util = require('util');

var model = Model.extend({});

var model = new model({
	db:'lb',
	collection: 'sites'
});

model.schema({
	title: {type: 'String'},
	domain: {type: 'String'}
});

module.exports = model;
