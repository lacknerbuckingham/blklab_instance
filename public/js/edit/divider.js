BlkLab.DividerBlock = BlkLab.Block.extend({
	element: null,
	type: 'divider',

	actions: {},

	create: function(){
		this.element = Coffea.create('section', {'class':'blklab-block divider'});
		this.element.data('type', 'divider');
	},

	render: function(container, insertionPoint, callback){
		var block = this.element;
		this.loadActions();

		if(!this.editing){
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
			}else{
				container.appendChild(block);
			}

			if(callback)
				callback.call(this);
		}
	}
});
