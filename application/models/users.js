var Model = require('blklab').Neo4jModel;
var util = require('util');
var Promise = require('promise');

var usersModel = Model.extend({
	findAll: function(){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASUSER]->(users)',
				'RETURN users'
			].join('\n')
			self.load(query, null, function(err, items) {
				resolve(items);
			});
		})
	},

	findByUsername: function(params){
		//Todo: Don't return users password over the api
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASUSER]->(user)',
				'WHERE user.username={username}',
				'RETURN user'
			].join('\n')

			self.findOne(query, params, function(err, items) {
				resolve(items);
			});
		})
	},

	createUser: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})',
				'CREATE (user:User {props})',
				'CREATE UNIQUE (website)-[:HASUSER]->(user)',
				'RETURN user'
			].join('\n')


			self.insert(query, params, function(err, result) {
				resolve(result);
			});
		});
	},

	updateUser: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;

			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASUSER]->(user)',
				'WHERE user.username={username}',
				'SET user += {props}',
				'RETURN user'
			].join('\n')

			self.update(query, params, function(err, items) {
				resolve(items);
			});
		});
	},

	deleteUser: function(params){
		var self = this;
		return new Promise(function (resolve, reject){
			var host = require('../../config').sitename;
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[rel:HASUSER]->(user)',
				'WHERE user.username={username}',
				'DELETE rel, user'
			].join('\n')


			self.del(query, params, function(err, items) {
				resolve(items);
			});
		});
	},
});

var model = new usersModel({
	db:'lb',
	collection: 'users'
});

model.schema({
	username: {type: 'String'},
	password: {type: 'String'},
	active: {type: 'Integer'}
});

module.exports = model;
