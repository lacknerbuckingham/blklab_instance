var collections = require('../controllers/collections');
var express = require('express');
var router = express.Router();

router.route('/').get(collections.all);
router.route('/').post(collections.add);
router.route('/:id').get(collections.single);
router.route('/:id').put(collections.update);
router.route('/:id').delete(collections.del);

module.exports = router;
