var blocks = require('../controllers/blocks');
var express = require('express');
var router = express.Router();

router.route('/').get(blocks.all);
router.route('/').post(blocks.add);
router.route('/:id').get(blocks.single);
router.route('/:id').put(blocks.update);
router.route('/:id').delete(blocks.del);

module.exports = router;
