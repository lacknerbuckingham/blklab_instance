var Model = require('blklab').Neo4jModel;
var util = require('util');

var mediaModel = Model.extend({});

var model = new mediaModel({
	db:'lb',
	collection: 'media'
});

model.schema({
	title: {type: 'String'},
	src: {type: 'String'},
	gravity: {type: 'String'},
	caption: {type: 'String'}
});

module.exports = model;
