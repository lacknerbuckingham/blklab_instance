var Model = require('blklab').Model;
var util = require('util');

var collectionsModel = Model.extend({});

var model = new collectionsModel({
	db:'lb',
	collection: 'collections'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
