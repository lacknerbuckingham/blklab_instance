this["templates"] = this["templates"] || {};

this["templates"]["./public/js/templates/config.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<section id=\"config\" class=\"col span_1_of_1\"><div class=\"inner\">Config</div></section>\n";
  },"useData":true});



this["templates"]["./public/js/templates/dashboard.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)));
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<section id=\"dashboard\" class=\"col span_1_of_1\">";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "<div class=\"inner\">Dashboard</div></section>\n";
},"useData":true});



this["templates"]["./public/js/templates/edit_gallery.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<img src=\""
    + escapeExpression(((helper = (helper = helpers.preview || (depth0 != null ? depth0.preview : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"preview","hash":{},"data":data}) : helper)))
    + "\">\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<h2><span class=\"fa fa-photo\"></span> Edit Gallery Block</h2>\n\n<div class=\"inner\">\n	<section class=\"left fifty\">\n		<div class=\"preview\">\n			<div class=\"blklab-gallery-preview\" id=\"blklab-gallery-preview\">\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\n		</div>\n		\n		<div class=\"tags\">\n			<span blklab-click=\"create_tag\">+ Create Tag</span>\n		</div>\n	</section>\n	\n	<section class=\"right fifty\">\n		<div class=\"library_tools\">\n			<div class=\"left\"><span class=\"fa fa-plus new_file\" blklab-change=\"select_image\"><input type=\"file\" id=\"new_file\" multiple></span></div>\n			<div class=\"right\"><span class=\"fa fa-th selected change_view\" id=\"tile_view\" data-view=\"tile_view\"></span> <span class=\"fa fa-list change_view\" id=\"list_view\" data-view=\"list_view\"></span></div>\n			<div class=\"clear\"></div>\n		</div>\n		<div id=\"file_library\" class=\"tiled\"></div>\n	</section>\n	<div class=\"clear\"></div>	\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"tools\">\n	<input type=\"button\" class=\"submit\" value=\"Change\"> <input type=\"button\" class=\"remove bad\" value=\"Remove Block\"> <input type=\"button\" class=\"cancel\" value=\"Done\">\n</div>		\n	\n\n";
},"useData":true});



this["templates"]["./public/js/templates/edit_image.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<h2><span class=\"fa fa-photo\"></span> Edit Image Block</h2>\n\n<div class=\"inner\">\n	<section class=\"left fifty\">\n		<div class=\"preview\">\n			<div class=\"thumb\">\n				<img src=\""
    + escapeExpression(((helper = (helper = helpers.preview || (depth0 != null ? depth0.preview : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"preview","hash":{},"data":data}) : helper)))
    + "\" id=\"preview\">\n			</div>\n		</div>\n		\n		<div class=\"tags\">\n			<span blklab-click=\"create_tag\">+ Create Tag</span>\n		</div>\n	</section>\n	\n	<section class=\"right fifty\">\n		<div class=\"library_tools\">\n			<div class=\"left\"><span class=\"fa fa-plus new_file\" blklab-change=\"select_image\"><input type=\"file\" id=\"new_file\" multiple></span></div>\n			<div class=\"right\"><span class=\"fa fa-th selected change_view\" id=\"tile_view\" data-view=\"tile_view\"></span> <span class=\"fa fa-list change_view\" id=\"list_view\" data-view=\"list_view\"></span></div>\n			<div class=\"clear\"></div>\n		</div>\n		<div id=\"file_library\" class=\"tiled\"></div>\n	</section>\n	<div class=\"clear\"></div>	\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"tools\">\n	<input type=\"button\" class=\"submit\" value=\"Change\"> <input type=\"button\" class=\"remove bad\" value=\"Remove Block\"> <input type=\"button\" class=\"cancel\" value=\"Done\">\n</div>		\n	\n\n";
},"useData":true});



this["templates"]["./public/js/templates/edit_text.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<h2><span class=\"fa blklab-text-icon\">a</span> Edit Text Block</h2>\n\n<div class=\"inner\">\n	<section class=\"left fifty\">\n		<div class=\"preview\">\n			<div class=\"thumb\">\n				<img src=\""
    + escapeExpression(((helper = (helper = helpers.preview || (depth0 != null ? depth0.preview : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"preview","hash":{},"data":data}) : helper)))
    + "\" id=\"preview\">\n			</div>\n		</div>\n		\n		<div class=\"tags\">\n			<span blklab-click=\"create_tag\">+ Create Tag</span>\n		</div>\n	</section>\n	\n	<section class=\"right fifty\">\n		<div class=\"library_tools\">\n			<div class=\"left\"><span class=\"fa fa-plus new_file\" blklab-change=\"select_image\"><input type=\"file\" id=\"new_file\" multiple></span></div>\n			<div class=\"right\"><span class=\"fa fa-th selected change_view\" id=\"tile_view\" data-view=\"tile_view\"></span> <span class=\"fa fa-list change_view\" id=\"list_view\" data-view=\"list_view\"></span></div>\n			<div class=\"clear\"></div>\n		</div>\n		<div id=\"file_library\" class=\"tiled\"></div>\n	</section>\n	<div class=\"clear\"></div>	\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"tools\">\n	<input type=\"button\" class=\"submit\" value=\"Change\"> <input type=\"button\" class=\"remove bad\" value=\"Remove Block\"> <input type=\"button\" class=\"cancel\" value=\"Done\">\n</div>";
},"useData":true});



this["templates"]["./public/js/templates/edit_video.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<h2><span class=\"fa fa-film\"></span> Edit Video Block</h2>\n\n<div class=\"inner\">\n	<section class=\"left fifty\">\n		<div class=\"preview\">\n			<div class=\"thumb\">\n				<img src=\""
    + escapeExpression(((helper = (helper = helpers.preview || (depth0 != null ? depth0.preview : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"preview","hash":{},"data":data}) : helper)))
    + "\" id=\"preview\">\n			</div>\n		</div>\n		\n		<div class=\"tags\">\n			<span blklab-click=\"create_tag\">+ Create Tag</span>\n		</div>\n	</section>\n	\n	<section class=\"right fifty\">\n		<div class=\"library_tools\">\n			<div class=\"left\"><span class=\"fa fa-plus new_file\" blklab-change=\"select_image\"><input type=\"file\" id=\"new_file\" multiple></span></div>\n			<div class=\"right\"><span class=\"fa fa-th selected change_view\" id=\"tile_view\" data-view=\"tile_view\"></span> <span class=\"fa fa-list change_view\" id=\"list_view\" data-view=\"list_view\"></span></div>\n			<div class=\"clear\"></div>\n		</div>\n		<div id=\"file_library\" class=\"tiled\"></div>\n	</section>\n	<div class=\"clear\"></div>	\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"tools\">\n	<input type=\"button\" class=\"submit\" value=\"Change\"> <input type=\"button\" class=\"remove bad\" value=\"Remove Block\"> <input type=\"button\" class=\"cancel\" value=\"Done\">\n</div>		\n	\n\n\n";
},"useData":true});



this["templates"]["./public/js/templates/main_nav.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li class=\"main_link\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" blklab-drag=\"sort_link\" blklab-drop=\"drop_link\" blklab_dragover=\"link_over\"  blklab_dragenter=\"link_enter\" draggable=\"true\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\n	<div class=\"tools\"><a href=\"/"
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\"><span class=\"fa fa-pencil good\"></span></a> <span class=\"fa fa-level-down\" blklab-click=\"show_collection\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" data-title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\"></span> <span class=\"fa fa-times bad\" blklab-click=\"delete_link\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\"></span>\n	</div>\n</li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<li class=\"inactive\">Main Nav:</li>\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});



this["templates"]["./public/js/templates/media.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "            <li id=\"row_"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\" blklab-click=\"select\">\n				<!--"
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\n                <div class=\"tools\"><span class=\"fa fa-times bad\" blklab-click=\"delete_image\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\"></span>\n                </div>-->\n				<img class=\"lazy images\" data-src=\""
    + escapeExpression(((helper = (helper = helpers.uri || (depth0 != null ? depth0.uri : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"uri","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "-thumb"
    + escapeExpression(((helper = (helper = helpers.extension || (depth0 != null ? depth0.extension : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"extension","hash":{},"data":data}) : helper)))
    + "\" data-tooltip=\""
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.uri || (depth0 != null ? depth0.uri : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"uri","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "-thumb"
    + escapeExpression(((helper = (helper = helpers.extension || (depth0 != null ? depth0.extension : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"extension","hash":{},"data":data}) : helper)))
    + "\">\n            </li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<section id=\"media\" class=\"col span_1_of_1\">\n	<div class=\"inner\">Media</div>\n	<section id=\"files\" blklab-drop=\"drop_image\" blklab-dragenter=\"list_enter\" blklab-dragover=\"list_over\" blklab-dragover=\"list_over\" blklab-dragleave=\"list_leave\">\n		<div class=\"toolbar\">\n			<span class=\"fa fa-plus new_file\" blklab-change=\"select_image\"><input type=\"file\" id=\"new_file\" multiple></span>\n			<span class=\"toolbar\" data-tooltip=\"Delete Selected\"><span class=\"fa fa-trash\" blklab-click=\"delete_selected\"></span></span>\n			\n			<div class=\"right\"><span class=\"fa fa-th selected change_view\" id=\"tile_view\" data-view=\"tile_view\"></span> <span class=\"fa fa-list change_view\" id=\"list_view\" data-view=\"list_view\"></span></div>				\n			<div class=\"search\">\n				<input type=\"text\" id=\"search_pages\" data-type=\"search\"><span class=\"fa fa-search\" blklab-click=\"search_pages\"></span>\n			</div>\n		</div>\n		<ul id=\"file-list\">\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</ul>				\n	</section>\n</section>\n";
},"useData":true});



this["templates"]["./public/js/templates/media_library.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "	<img class=\"lazy toolbar images\" data-src=\""
    + escapeExpression(((helper = (helper = helpers.uri || (depth0 != null ? depth0.uri : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"uri","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "-thumb"
    + escapeExpression(((helper = (helper = helpers.extension || (depth0 != null ? depth0.extension : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"extension","hash":{},"data":data}) : helper)))
    + "\" data-tooltip=\""
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.uri || (depth0 != null ? depth0.uri : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"uri","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "-thumb"
    + escapeExpression(((helper = (helper = helpers.extension || (depth0 != null ? depth0.extension : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"extension","hash":{},"data":data}) : helper)))
    + "\">\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});



this["templates"]["./public/js/templates/media_library_list.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "	<li class=\"images\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.uri || (depth0 != null ? depth0.uri : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"uri","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"filename","hash":{},"data":data}) : helper)))
    + "-thumb"
    + escapeExpression(((helper = (helper = helpers.extension || (depth0 != null ? depth0.extension : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"extension","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "</li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<ul>\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</ul>";
},"useData":true});



this["templates"]["./public/js/templates/media_row.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li id=\"row_"
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\">\n	"
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\n	<div class=\"tools\"><span class=\"fa fa-times bad\" blklab-click=\"delete_image\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.original || (depth0 != null ? depth0.original : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"original","hash":{},"data":data}) : helper)))
    + "\"></span></div>\n</li>";
},"useData":true});



this["templates"]["./public/js/templates/messages.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<section id=\"media\" class=\"col span_1_of_1\"><div class=\"inner\">Messages</div></section>\n";
  },"useData":true});



this["templates"]["./public/js/templates/meta.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<form method=\"put\" action=\"/api/pages/:id\">\n	<h2><span class=\"fa fa-info-circle\"></span> Edit Page Meta</h2>\n	<div class=\"inner meta\">\n		<section class=\"left fifty\">\n			<div class=\"preview\">\n				<label for=\"identifier\">Thumbnail:</label>\n				<div class=\"thumb\"></div>\n			</div>\n		</section>\n\n		<section class=\"right fifty\">\n			<div class=\"row\">\n				<label for=\"nav_title\">Navigation Title:</label>\n				<input type=\"text\" name=\"nav_title\" placeholder=\"\" id=\"nav_title\" value=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\" />\n			</div>\n\n			<div class=\"row\">\n				<label for=\"title\">Page Title:</label>\n				<input type=\"text\" name=\"title\" placeholder=\"\" id=\"title\" value=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\" />\n			</div>\n\n			<div class=\"row\">\n				<label for=\"url\">Url:</label>\n				<input type=\"text\" name=\"url\" placeholder=\"ex. test-page\" id=\"url\" value=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" />\n				<span class=\"form_hint\">The unique location slug for this page.</span>\n			</div>\n		</section>\n		<div class=\"clear\"></div>	\n	</div>\n\n	<div class=\"clear\"></div>\n\n	<div class=\"tools\">\n		<input type=\"button\" class=\"submit\" value=\"Save\">  <input type=\"button\" class=\"cancel\" value=\"Done\">\n	</div>	\n\n	<div class=\"clear\"></div>\n</form>\n";
},"useData":true});



this["templates"]["./public/js/templates/stack.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\n            <li id=\"page-"
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" class=\"page\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" data-title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\" blklab-drag=\"drag_page\" draggable=\"true\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\n                <div class=\"tools\"><a href=\"/"
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\"><span class=\"fa fa-pencil\"></span></a><span class=\"fa fa-times bad\" blklab-click=\"delete_page\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\"></span>\n                </div>\n            </li>";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<section id=\"pages\" class=\"col span_1_of_4\">\n    <div class=\"inner\">\n        <ul id=\"page-list\">\n            <li class=\"header\"><span class=\"fa fa-plus\" blklab-click=\"new_page\"></span>\n                <div class=\"search\">\n                    <input type=\"text\" id=\"search_pages\" data-type=\"search\"><span class=\"fa fa-search\" blklab-click=\"search_pages\"></span>\n                </div>\n            </li>";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</ul>\n    </div>\n</section>\n<section id=\"stack\" class=\"col span_3_of_4\">\n    <div class=\"inner\">\n        <ul class=\"header\" id=\"main_nav\" blklab-drop=\"drop_page\" blklab-dragenter=\"page_enter\" blklab-dragover=\"page_over\" blklab-dragleave=\"page_leave\">\n        </ul>\n		<div id=\"path\"></div>\n		<ul id=\"sub_nav\" blklab-drop=\"drop_subpage\" blklab-dragenter=\"page_enter\" blklab-dragover=\"page_over\" blklab-dragleave=\"page_leave\"></ul>\n    </div>\n</section>\n";
},"useData":true});



this["templates"]["./public/js/templates/sub_nav.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li class=\"sub_link\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" blklab-drag=\"sort_link\" blklab-drop=\"drop_sub_link\" blklab_dragover=\"link_over\" blklab_dragenter=\"link_enter\" draggable=\"true\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\n	<div class=\"tools\"><a href=\"/"
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\"><span class=\"fa fa-pencil good\"></span></a> <span class=\"fa fa-level-down\" blklab-click=\"show_collection\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" data-title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\"></span> <span class=\"fa fa-times bad\" blklab-click=\"delete_sub_link\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\"></span>\n	</div>\n</li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});



this["templates"]["./public/js/templates/users.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li id=\"page-"
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + " <div class=\"tools\"><span class=\"fa fa-times bad\" blklab-click=\"delete_user\" data-username=\""
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\"></span></div></li>";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<section id=\"users\" class=\"col span_1_of_1\"><div class=\"inner\"><ul id=\"user-list\"><li class=\"header\"><span class=\"fa fa-plus\" blklab-click=\"new_user\"></span> <input type=\"text\" class=\"search\" blklab-typing=\"search_users\"></li>";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</ul></div></section>\n";
},"useData":true});