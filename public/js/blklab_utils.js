(function(window, document) {
    'use strict';

	BlkLab.imageProcessor = 'http://imageprocessor/';

    //XHR object
    window.XHR = function() {
        this.http = new XMLHttpRequest();
        this.callback = null;
        this.method = 'GET';
        this.querystring = null;
        this.content_type = "application/x-www-form-urlencoded";
        this.upload = null;

        this.listeners = function(listeners) {
            var lxhr = Coffea.find(this.http);
            for (var l in listeners) {
                if (l == 'progress') {
                    Coffea.find(this.upload).ev(l, listeners[l]);
                } else {
                    lxhr.ev(l, listeners[l]);
                }
            }
        };

        this.headers = function(fields) {
            for (var key in fields) {
                this.http.setRequestHeader(key, fields[key]);
            }
            if (this.content_type !== "") {
                this.http.setRequestHeader("Content-Type", this.content_type);
            }
        };
    };

    //Call the XHR object and handle the response
    XHR.prototype.call = function(url) {
        var xhr = this;
        return new Promise(function(resolve, reject) {
			xhr.http.open(xhr.method, url);
			if(xhr.content_type !== "") {
                xhr.http.setRequestHeader("Content-Type", xhr.content_type);
            }
            xhr.http.onload = function() {
                if (xhr.http.status === 200) {
                    resolve(xhr.http);
                }else{
                    reject(new Error(xhr.http.statusText));
                }
            };

            xhr.http.onerror = function() {
                reject(new Error("Network Error"));
            };

            xhr.http.send(xhr.querystring || null);
        });
    };


    //Convenience methods for calling an AJAX GET, POST, PUT and DELETE requests
    //TODO: abstract internal function to generalize the calls
    window.get = function(url, data, success, err) {
        success = arguments.length < 4 ? data : success;
        err = arguments.length < 4 ? success : err;
        var req = new XHR();
        return req.call(url).then(success, err);
    };

    window.post = function(params) {
		//url, data, success, err
        var success = params.success;
        var err = params.fail;
        var req = new XHR();
		if(params.dataType){
			req.content_type = params.dataType;
		}else{
			req.content_type = 'application/json';
		}
        req.method = 'POST';
        req.querystring = params.data;
		if(success || err){
        	return req.call(params.url).then(success, err);
		}else{
			return req.call(params.url);
		}
    };

    window.put = function(params) {
        var success = params.success;
        var err = params.fail;
        var req = new XHR();
		if(params.dataType){
			req.content_type = params.dataType;
		}else{
			req.content_type = 'application/json';
		}
        req.method = 'PUT';
        req.querystring = params.data;
		if(success || err){
        	return req.call(params.url).then(success, err);
		}else{
			return req.call(params.url);
		}
    };

    window.del = function(params) {
		var success = params.success || function(){};
        var err = params.fail || function(){};
        var req = new XHR();
		if(params.dataType){
			req.content_type = params.dataType;
		}else{
			req.content_type = 'application/json';
		}
        req.method = 'DELETE';
        req.querystring = params.data;		
        if(success || err){
        	return req.call(params.url).then(success, err);
		}else{
			return req.call(params.url);
		}
    };
	
	var agent = navigator.userAgent.toLowerCase();
	BlkLab.Browser = {
		version: (agent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
		is_chrome : /chrome/.test(agent),
		is_safari : /webkit/.test(agent) && !/chrome/.test(agent),
		is_opera : /opera/.test(agent),
		is_ie : /msie/.test(agent) && !/opera/.test(agent),
		is_mozilla : /mozilla/.test(agent) && !/(compatible|webkit)/.test(agent),
		is_ipad : /iPad/i.test(agent),
		is_iphone : /iPhone/i.test(agent)
	}

    BlkLab.History = {
        type: 'push',
        base: '',
		callback: function(){},

        start: function(params) {
            this.base = location.protocol + '//' + location.hostname;
			if(params && params.callback){
				this.callback = params.callback;
			}
            if (params && params.type == 'hash') {
                this.type = "hash";
            } else {
                this.type = "push";
                var pushState = window.history.pushState;
                window.history.pushState = function(state) {
                    var ret = pushState.apply(window.history, arguments);
                    if (typeof window.history.onpushstate == "function") {
                        window.history.onpushstate({
                            state: state
                        });
                    }
                    return ret;
                }
            }			
            document.documentElement.addEventListener("click", this.intercept, false);
        },

        intercept: function(e) {
            var target = Evnt.target(e);
            if ((target.tagName == 'A' || target.getAttribute('data-href')) && target.getAttribute('data-target') != 'new') {
                e.stopPropagation();
                e.preventDefault();
				var tmp = target.href ? target.href : target.getAttribute('data-href');
                var href = tmp.replace(BlkLab.History.base, '');
				var cur = location.href.replace(BlkLab.History.base, '');
				if(cur != href){
					if (BlkLab.History.type == 'push') {
						window.history.pushState("", "", href);
					} else if (BlkLab.History.type == 'hash') {
						location.hash = href;
					}
				}
				if(BlkLab.History.callback){
					BlkLab.History.callback.call(target, href);
				}
                return false;
            }else if(target.getAttribute('data-href') && target.getAttribute != 'new'){
				location.href = target.getAttribute('data-href');
			}
        }
    }


	BlkLab.Search = function(array, predicate){
		var matchingIndices = [];

		for(var j = 0; j < array.length; j++)
		{
			if(predicate(array[j]))
			   matchingIndices.push(array[j]);
		}

		return matchingIndices;
	}

    //Base Event
    function E() {}
    //Expose Event
    window.Evnt = new E();

    //Return Event target
    Evnt.target = function(e) {
        e = e || window.event;
        return e.target || e.srcElement;
    };

	window.Files = {
		timer: null,
		previous_bytes_loaded: 0,
		bytes_uploaded: 0,
		bytes_total: 0,
		placeholder: null,
		progress_bar: null,
		multiple: false,
		files:[],
		filesCompleted:0,
		progressBars:[],

		readFile: function(f, callback){
			var reader = new FileReader();
			reader.onload = callback;
			if(typeof f === 'object'){
				reader.readAsDataURL(f);
			}
		},

		handle: function(files, scope){
			this.files = files;
			var len = files.length;
			var i;
			var form_data;
			var file;
			var url = BlkLab.imageProcessor + '/images/upload';

			this.progress_bar = BlkLab.create('div', {id: 'progress'});
			scope.append(this.progress_bar);

			if(len == 1){
				file = files[0];
				scope.innerHTML = '';
				scope.id = 'block_' + file.name;
				var placeholder = BlkLab.create('div', {'class':'img_placeholder p1'});
				placeholder.css({width:'90%'});
				Files.progress_bar = BlkLab.create('div', {'class':'progress_bar p1 loader loader-quart show'});
				placeholder.id = file.name;
				placeholder.append(Files.progress_bar);
				scope.appendChild(placeholder);

				form_data = new FormData();
				form_data.append('files', file);

				Files.start(url, form_data);

				Files.timer = setInterval(Files.update, 300);

			}else if(len > 1){
				for(i=0; i<len; i++){
					file = files[i];
					scope.id = 'block_' + file.name;
					var placeholder = BlkLab.create('div', {'class':'img_placeholder p' + i});
					var progress = BlkLab.create('div', {'class':'progress_bar p loader loader-quart show' + i});
					placeholder.id = file.name;
					placeholder.append(progress);
					scope.appendChild(placeholder);

					form_data = new FormData();
					form_data.append('files', file);

					Files.start(url, form_data);

					Files.timer = setInterval(Files.update, 300);
				}
			}
		},

		start: function(url, form_data){
			var oReq = new XMLHttpRequest();
			oReq.open('POST', url, true);
			oReq.upload.addEventListener("progress", Files.progress, false);
			oReq.addEventListener("load", Files.load, false);
			oReq.addEventListener("error", Files.error, false);
			oReq.addEventListener("abort", Files.abort, false);
			oReq.send(form_data);
		},

		update: function() {
			var current_bytes = Files.bytes_uploaded;
			Files.previous_bytes_loaded = current_bytes;
		},

		progress: function(e) {
			if (e.lengthComputable) {
				Files.bytes_uploaded = e.loaded;
				Files.bytes_total = e.total;
				var precent_complete = Math.round(e.loaded / e.total);
				//var bytes_transfered = Files.bytes_to_size(Files.bytes_uploaded);
				Files.progress_bar.css({
					width: (precent_complete * 4).toString() + 'px'
				});

				if (precent_complete == 100) {
					Files.progress_bar.css({
						width: '400px'
					});
				}
			} else {
				Files.progress_bar.html('unable to compute');
			}
		},

		load : function (e) {
			Files.filesCompleted++;
			if(Files.filesCompleted == Files.files.length){
				Files.finish(e);
			}
		},

		finish: function(e) {
			var response = e.target.responseText;
			var hash = JSON.parse(response);
			var i;
			if(this.files.length > 1){
				for(i=0;i<hash.files.length;i++){
					console.log(hash.files[i]);
				}
			}else{
				var file = hash.files[0];
				var original = file.original;
				var placeholder = $(original);
				var block = $('block_' + original);
				var img = BlkLab.create('img', {'class':'lazy'});
				var tries = 0;
				var timeout;

				var loadImg = function(){
					clearTimeout(timeout);

					img.data('src', file.uri + '/' + file.filename + '-lg' + file.extension);

					img.src = file.tmp;
					img.onload = function(){
						placeholder.destroy();
						var height = img.height;
						var width = img.width;
						if(width>height){
							img.add_class('wide-img');
						}else{
							img.add_class('tall-img');
						}
						img.width = width;
						img.height = height;
						block.innerHTML = '';
						block.append(img);
					}
				}

				loadImg();
				img.onerror = function(){
					tries++;
					clearTimeout(timeout);
					timeout = setTimeout(loadImg, 1000);
				}
			}

			//Files.progress_bar.destroy();
			clearInterval(Files.timer);

			this.uploading = false;
			this.filesCompleted = 0;
			this.files = [];
		},

		error: function(e) {
			clearInterval(Files.timer);
		},

		abort: function(e) {
			clearInterval(Files.timer);
		}
	}

	window.Videos = {
		timer: null,
		previous_bytes_loaded: 0,
		bytes_uploaded: 0,
		bytes_total: 0,
		placeholder: null,
		progress_bar: null,
		multiple: false,

		readFile: function(f, callback){
			var reader = new FileReader();
			reader.onload = callback;
			if(typeof f === 'object'){
				reader.readAsDataURL(f);
			}
		},

		handle: function(files, scope){
			var len = files.length;
			var i;
			var form_data;
			var file;
			var url = BlkLab.imageProcessor + '/videos/upload';

			if(len == 1){
				file = files[0];
				scope.innerHTML = '';
				scope.id = 'block_' + file.name;
				var placeholder = BlkLab.create('div', {'class':'img_placeholder p1'});
				placeholder.css({width:'90%'});
				Videos.progress_bar = BlkLab.create('div', {'class':'progress_bar p1 loader loader-quart show'});
				placeholder.id = file.name;
				placeholder.append(Files.progress_bar);
				scope.appendChild(placeholder);

				form_data = new FormData();
				form_data.append('files', file);

				Videos.start(url, form_data);

				Videos.timer = setInterval(Files.update, 300);

			}else if(len > 1){
				for(i=0; i<len; i++){
					file = files[i];
					scope.id = 'block_' + file.name;
					var placeholder = BlkLab.create('div', {'class':'img_placeholder p' + i});
					var progress = BlkLab.create('div', {'class':'progress_bar p loader loader-quart show' + i});
					placeholder.id = file.name;
					placeholder.append(progress);
					scope.appendChild(placeholder);

					form_data = new FormData();
					form_data.append('files', file);

					Videos.start(url, form_data);

					Videos.timer = setInterval(Videos.update, 300);
				}
			}
		},

		start: function(url, form_data){
			var oReq = new XMLHttpRequest();
			oReq.open('POST', url, true);
			oReq.upload.onprogress = Videos.progress;
			oReq.addEventListener("load", Videos.load, false);
			oReq.addEventListener("error", Videos.error, false);
			oReq.addEventListener("abort", Videos.abort, false);
			oReq.send(form_data);
		},

		update: function() {
			var current_bytes = Videos.bytes_uploaded;
			Videos.previous_bytes_loaded = current_bytes;
		},

		progress: function(e) {
			console.log(e);
			if (e.lengthComputable) {
				Videos.bytes_uploaded = e.loaded;
				Videos.bytes_total = e.total;
				var precent_complete = Math.round(e.loaded * 100 / e.total);
				var bytes_transfered = upload.bytes_to_size(Videos.bytes_uploaded);
				Videos.progress_bar.css({
					width: (precent_complete * 4).toString() + 'px'
				});

				if (precent_complete == 100) {}
			} else {
				Videos.progress_bar.html('unable to compute');
			}
		},

		load : function (e) {
			console.log(e);
			Videos.finish(e);
		},

		finish: function(e) {
			var response = e.target.responseText;
			var hash = JSON.parse(response);
			var i;
			for(i=0;i<hash.files.length;i++){
				var file = hash.files[i];
				var original = file.original;
				var placeholder = $(original);
				var block = $('block_' + original);
				var video = BlkLab.create('video', {'poster':file.tmp, autoplay: 'true', loop: 'true'});
				//<source src="http://imageprocessor/assets/videos/snow.webm" type="video/webm">
				//<source src="http://imageprocessor/assets/videos/snow.mp4" type="video/mp4">
				var webm = BlkLab.create('source', {'src': BlkLab.imageProcessor + '/assets/videos/' + file.filename + '.webm', type: 'video/webm'});
				var mp4 = BlkLab.create('source', {'src': BlkLab.imageProcessor + '/assets/videos/' + file.filename + '.mp4', type: 'video/mp4'});
				var ogg = BlkLab.create('source', {'src': BlkLab.imageProcessor + '/assets/videos/' + file.filename + '.ogv', type: 'video/ogv'});
				video.append(webm);
				video.append(mp4);
				video.append(ogg);
				var tries = 0;
				var timeout;
				var img = new Image();

				var loadImg = function(){
					clearTimeout(timeout);

					img.src = file.tmp;
					img.onload = function(){
						placeholder.destroy();
						block.innerHTML = '';
						block.append(video);
					}
				}

				loadImg();
				img.onerror = function(){
					tries++;
					clearTimeout(timeout);
					timeout = setTimeout(loadImg, 1000);
				}

			}

			//Files.progress_bar.destroy();
			clearInterval(Files.timer);

			this.uploading = false;
		},

		error: function(e) {
			o('error2').show();
			clearInterval(Files.timer);
		},

		abort: function(e) {
			o('abort').show();
			clearInterval(Files.timer);
		}
	}
	
	window.Activity = function(table){
		var alrt = BlkLab.create('div', {'class': 'blklab-activity'});
		alrt.append(table);
		document.documentElement.appendChild(alrt);
		alrt.add_class('open');
		return alrt;
	}
	
	window.Alert = function(msg){
		var alrt = BlkLab.create('div', {'class': 'blklab-msg'});
		alrt.innerHTML = msg;
		alrt.click = function(){alrt.destroy();}
		document.documentElement.appendChild(alrt);
		var t = setTimeout(function(){
			clearTimeout(t);
			alrt.destroy();
		}, 5000);
	}
	
	BlkLab.Animation = function(duration, type){
		this.elem = null;
		this.startTime = undefined;
		this.time = undefined;
		this.duration = duration || 500;
		this.type = type || null;
		this.target = 0;
		this.start = 0;
		this.prop = null;
		this.callback = null;

		this.swing = function(p, n, firstNum, diff){
			return ((-Math.cos(p * Math.PI) / 2) + 0.5) * diff + firstNum;
		}

		this.loop = function(){
			this.time = Date.now();
			if(this.startTime === undefined)
				this.startTime = this.time;

			if(this.started){
				var dur = this.duration + this.startTime;
				if(this.time >= dur){
					this.started = false;
					this.time = undefined;
					this.startTime = undefined;
					if(this.type == 'window'){
						window.scrollTo(0, this.target);
					}else if(this.prop == 'opacity'){
						this.elem.style[this.prop] = val						
					}else{
						this.elem.style[this.prop] = this.target + 'px'
					}

					if(this.callback)
						this.callback();
				}else{
					var curTime = this.time - this.startTime;
					var curPos = curTime / this.duration;
					var diff = (this.target - this.start);
					var ease = this.swing(curPos, curTime, 0, 1, this.duration);
					var val = Math.ceil(this.start + ((this.target - this.start) * ease));
					//console.log(val)
					if(this.type == 'window'){
						window.scrollTo(0, val);
					}else if(this.prop == 'opacity'){
						this.elem.style[this.prop] = val						
					}else{
						this.elem.style[this.prop] = val + 'px'
					}
				}
			}
		}

		this.run = function(elem, prop, start, end, callback){
			var that = this;
			this.elem = elem;
			this.start = start;
			this.target = end;
			this.prop = prop;
			this.callback = callback;
			this.started = true;
			(function animloop(){
				window.requestAnimationFrame(animloop);
				that.loop();
			})();
		}
	}
	
	XMLHttpRequestUpload.prototype.file = null;
	
	BlkLab.Frontend = {};
	
	BlkLab.Frontend.Uploader = {
		bytes_uploaded: 0,
        bytes_total: 0,
        previous_bytes_loaded: 0,
        max_file_size: 1048576,
        timer: 0,
        total_file_size: '',
        uploading: false,
        queue: {},
        cur: 1,
		total:0,
        cur_total:0,
        callback: null,
        progress_bar: '',
		dialog:null,
		running: [],
		table:null,
		header:null,
		close:null,
		urls: {
			'img': BlkLab.imageProcessor + '/images/upload',
			'video': BlkLab.imageProcessor + '/videos/upload',			
			'pdf': BlkLab.imageProcessor + '/documents/upload',						
			'doc': BlkLab.imageProcessor + '/documents/upload',						
			'ppt': BlkLab.imageProcessor + '/documents/upload',									
		},
		
		check_type: function(file) {
			var file_type;
			
            var imgfilter = /^(image\/bmp|image\/gif|image\/jpeg|image\/png|image\/tiff)$/i;
            if (imgfilter.test(file.type)) {
                return BlkLab.Frontend.Uploader.urls['img'];
            }
			
			var videofilter = /^(video\/mp4|video\/quicktime|video\/x-msvideo|video\/x-ms-wmv)$/i;
            if (videofilter.test(file.type)) {
                return BlkLab.Frontend.Uploader.urls['video'];
            }

            if (file.type == 'application/pdf') {
                return BlkLab.Frontend.Uploader.urls['pdf'];
            }

            if (file.type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.type == 'application/msword') {
                return BlkLab.Frontend.Uploader.urls['doc'];
            }

            if (file.type == 'application/vnd.ms-powerpoint' || file.type == 'application/mspowerpoint' || file.type == 'application/powerpoint' || file.type == 'application/x-mspowerpoint') {
                return BlkLab.Frontend.Uploader.urls['ppt'];
            }

            return false;
        },
		
        seconds_to_time: function (secs){
            var hr = Math.floor(secs / 3600);
            var min = Math.floor((secs - (hr * 3600))/60);
            var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

            if (hr < 10) {
                hr = "0" + hr;
            }
            if (min < 10) {
                min = "0" + min;
            }
            if (sec < 10) {
                sec = "0" + sec;
            }
            if (hr) {
                hr = "00";
            }
            return hr + ':' + min + ':' + sec;
        },

        bytes_to_size: function(bytes){
            var sizes = ['Bytes', 'KB', 'MB', 'GB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        },
        
		add_to_queue: function(file, meta){
			meta = meta ? meta : {};
			meta.uuid = BlkLab.Utils.generateUUID();
			meta.bytes_loaded = 0;
			meta.previous_bytes_loaded = 0;
			BlkLab.Frontend.Uploader.queue[file.name] = {file: file, meta:meta};
			BlkLab.Frontend.Uploader.cur_total++;
		},
		
		start: function(){
			BlkLab.Frontend.Uploader.total = BlkLab.Frontend.Uploader.cur_total
			
			var key;
			var row;
			var url;
			for(key in BlkLab.Frontend.Uploader.queue){
				var file = BlkLab.Frontend.Uploader.queue[key];
				if(BlkLab.Frontend.Uploader.running.indexOf(file.meta.name) == -1){
					url = BlkLab.Frontend.Uploader.check_type(file.file);
					if(url){
						this.process(file, url);
					}else{
						delete BlkLab.Frontend.Uploader.queue[key];
						BlkLab.Frontend.Uploader.cur_total--;								
						if(file.meta.callback)
							file.meta.callback()
					}
				}
			}
		},
		
        process: function(file, url){
			BlkLab.Frontend.Uploader.running.push(file.meta.name);
			
            var form_data = new FormData(); 
	        form_data.append('files',file.file);
            
           	var oReq = new XMLHttpRequest();
			oReq.open('POST', url, true);
			oReq.upload.file = file.meta;
			oReq.upload.onprogress = BlkLab.Frontend.Uploader.progress;
			oReq.addEventListener("load", BlkLab.Frontend.Uploader.load, false);
			oReq.addEventListener("error", BlkLab.Frontend.Uploader.error, false);
			oReq.addEventListener("abort", BlkLab.Frontend.Uploader.abort, false);
			oReq.send(form_data);
            
            BlkLab.Frontend.Uploader.timer = setInterval(BlkLab.Frontend.Uploader.update, 300);
        },
        
        update: function(){
			var key;			
			for(key in BlkLab.Frontend.Uploader.queue){
				var file = BlkLab.Frontend.Uploader.queue[key].meta;
				var current_bytes = file.bytes_uploaded;
				file.previous_bytes_loaded = current_bytes;
			}
        },

        progress: function(e){
			var file = e.target.file;			
			if(e.lengthComputable) {
                file.bytes_uploaded = e.loaded;
                file.bytes_total = e.total;
                var precent_complete = Math.round(e.loaded * 100 / e.total);
                var bytes_transfered = BlkLab.Frontend.Uploader.bytes_to_size(BlkLab.Frontend.Uploader.bytes_uploaded);
				if(file.progress_bar){
					file.progress_bar.css({
						width: (precent_complete).toString() + '%'
					});

					if (precent_complete == 100){
						file.row.css({
							background:'#000000'
						});
					}
				}
            } else {
				delete BlkLab.Frontend.Uploader.queue[file.name];
				delete BlkLab.Frontend.Uploader.running[file.name];
				if(file.callback)
					file.callback();
            }
        },

        load : function (e) {
			delete BlkLab.Frontend.Uploader.queue[e.target.upload.file.name];
			delete BlkLab.Frontend.Uploader.running[e.target.upload.file.name];			

			BlkLab.Frontend.Uploader.cur_total--;			
            BlkLab.Frontend.Uploader.finish(e);
        },
        
        finish: function(e){
        	var response = e.target.response;
            var hash = JSON.parse(response);
			
			
			if(BlkLab.Frontend.Uploader.cur_total == 0){
	            clearInterval(BlkLab.Frontend.Uploader.timer);
			}
            
            this.uploading = false;
            
            if(e.target.upload.file.callback){
            	e.target.upload.file.callback(e.target.upload.file, hash.files[0]);
			}
        },
        
        error: function(e){
            clearInterval(BlkLab.Frontend.Uploader.timer);
        },

        abort: function(e){
            clearInterval(BlkLab.Frontend.Uploader.timer);
        }  
	}
	
	XMLHttpRequestUpload.prototype.file = null;
	
	BlkLab.Uploader = {
		bytes_uploaded: 0,
        bytes_total: 0,
        previous_bytes_loaded: 0,
        max_file_size: 1048576,
        timer: 0,
        total_file_size: '',
        uploading: false,
        queue: {},
        cur: 1,
		total:0,
        cur_total:0,
        callback: null,
        progress_bar: '',
		dialog:null,
		running: [],
		table:null,
		header:null,
		close:null,
		urls: {
			'img': BlkLab.imageProcessor + '/images/upload',
			'video': BlkLab.imageProcessor + '/videos/upload',			
			'pdf': BlkLab.imageProcessor + '/documents/upload',						
			'doc': BlkLab.imageProcessor + '/documents/upload',						
			'ppt': BlkLab.imageProcessor + '/documents/upload',									
		},
		
		check_type: function(file) {
			var file_type;
			
            var imgfilter = /^(image\/bmp|image\/gif|image\/jpeg|image\/png|image\/tiff)$/i;
            if (imgfilter.test(file.type)) {
                return BlkLab.Uploader.urls['img'];
            }
			
			var videofilter = /^(video\/mp4|video\/quicktime|video\/x-msvideo|video\/x-ms-wmv)$/i;
            if (videofilter.test(file.type)) {
                return BlkLab.Uploader.urls['video'];
            }

            if (file.type == 'application/pdf') {
                return BlkLab.Uploader.urls['pdf'];
            }

            if (file.type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.type == 'application/msword') {
                return BlkLab.Uploader.urls['doc'];
            }

            if (file.type == 'application/vnd.ms-powerpoint' || file.type == 'application/mspowerpoint' || file.type == 'application/powerpoint' || file.type == 'application/x-mspowerpoint') {
                return BlkLab.Uploader.urls['ppt'];
            }

            return false;
        },
		
        seconds_to_time: function (secs){
            var hr = Math.floor(secs / 3600);
            var min = Math.floor((secs - (hr * 3600))/60);
            var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

            if (hr < 10) {
                hr = "0" + hr;
            }
            if (min < 10) {
                min = "0" + min;
            }
            if (sec < 10) {
                sec = "0" + sec;
            }
            if (hr) {
                hr = "00";
            }
            return hr + ':' + min + ':' + sec;
        },

        bytes_to_size: function(bytes){
            var sizes = ['Bytes', 'KB', 'MB', 'GB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        },
        
		add_to_queue: function(file, callback){
			BlkLab.Uploader.queue[file.name] = {file: file, meta:{uuid: BlkLab.Utils.generateUUID(), bytes_loaded:0, previous_bytes_loaded:0, name: file.name, callback:callback}};
			BlkLab.Uploader.cur_total++;
		},
		
		start: function(){
			BlkLab.Uploader.total = BlkLab.Uploader.cur_total
			if(!BlkLab.Uploader.table){
				BlkLab.Uploader.table = BlkLab.create('div', {'class':'table'});
				BlkLab.Uploader.header = BlkLab.create('h2', {'id':'activity_title'});
				BlkLab.Uploader.close = BlkLab.create('div', {'id':'close'});				
				BlkLab.Uploader.close.innerHTML = 'X';
				BlkLab.Uploader.table.append(BlkLab.Uploader.header);	
				BlkLab.Uploader.table.append(BlkLab.Uploader.close);					
			}
			BlkLab.Uploader.close.hide();
			BlkLab.Uploader.header.innerHTML = 'Uploading ' + BlkLab.Uploader.total + ' of ' + BlkLab.Uploader.cur_total + ' Files';

			
			var key;
			var row;
			var url;
			for(key in BlkLab.Uploader.queue){
				var file = BlkLab.Uploader.queue[key];
				if(BlkLab.Uploader.running.indexOf(file.meta.name) == -1){
					row = BlkLab.create('div', {'class':'row', id:file.meta.uuid});
					row.innerHTML = '<div class="text">' + key + ' | ' + this.bytes_to_size(file.file.size) + '</div>';
					BlkLab.Uploader.table.appendChild(row);
					file.meta.row = row;
					url = BlkLab.Uploader.check_type(file.file);
					if(url){
						this.process(file, url);
					}else{
						row.innerHTML += ' Failed!';
						row.add_class('fail');
						delete BlkLab.Uploader.queue[key];
						BlkLab.Uploader.cur_total--;								
						console.log(BlkLab.Uploader.queue);
						console.log(BlkLab.Uploader.cur_total);										
					}
				}
			}
			
			if(!this.dialog){
				var dialog = this.dialog = new Activity(BlkLab.Uploader.table);			
				BlkLab.Uploader.close.click(function(){
					dialog.parentNode.removeChild(dialog);
					BlkLab.Uploader.table = null;
					BlkLab.Uploader.header = null;
					BlkLab.Uploader.close = null;
					BlkLab.Uploader.dialog = null;
				});
			}
		},
		
        process: function(file, url){
			BlkLab.Uploader.running.push(file.meta.name);
            file.meta.progress_bar = BlkLab.create('div', {'class': 'progress_bar', id: file.meta.uuid}); 
			file.meta.row.append(file.meta.progress_bar);
			
            var form_data = new FormData(); 
	        form_data.append('files',file.file);
            
           	var oReq = new XMLHttpRequest();
			oReq.open('POST', url, true);
			oReq.upload.file = file.meta;
			oReq.upload.onprogress = BlkLab.Uploader.progress;
			oReq.addEventListener("load", BlkLab.Uploader.load, false);
			oReq.addEventListener("error", BlkLab.Uploader.error, false);
			oReq.addEventListener("abort", BlkLab.Uploader.abort, false);
			oReq.send(form_data);
            
            BlkLab.Uploader.timer = setInterval(BlkLab.Uploader.update, 300);
        },
        
        update: function(){
			var key;			
			for(key in BlkLab.Uploader.queue){
				var file = BlkLab.Uploader.queue[key].meta;
				var current_bytes = file.bytes_uploaded;
				file.previous_bytes_loaded = current_bytes;
			}
        },

        progress: function(e){
			var file = e.target.file;			
			if(e.lengthComputable) {
                file.bytes_uploaded = e.loaded;
                file.bytes_total = e.total;
                var precent_complete = Math.round(e.loaded * 100 / e.total);
                var bytes_transfered = BlkLab.Uploader.bytes_to_size(BlkLab.Uploader.bytes_uploaded);
                file.progress_bar.css({
                    width: (precent_complete).toString() + '%'
                });
                    
                if (precent_complete == 100){
					file.row.css({
						background:'#000000'
					});
				}
            } else {
				file.row.innerHTML += ' Failed!';
				file.row.add_class('fail');
				delete BlkLab.Uploader.queue[file.name];
				delete BlkLab.Uploader.running[file.name];
            }
        },

        load : function (e) {
			delete BlkLab.Uploader.queue[e.target.upload.file.name];
			delete BlkLab.Uploader.running[e.target.upload.file.name];			

			BlkLab.Uploader.cur_total--;			
            BlkLab.Uploader.finish(e);
        },
        
        finish: function(e){
        	var response = e.target.responseText;
            var hash = JSON.stringify(response);
			
			
			if(BlkLab.Uploader.cur_total == 0){
	            clearInterval(BlkLab.Uploader.timer);
	            BlkLab.Uploader.header.innerHTML = 'Uploading Complete';
				BlkLab.Uploader.close.show();
			}else{
				BlkLab.Uploader.header.innerHTML = 'Uploading ' + BlkLab.Uploader.cur_total + ' of ' + BlkLab.Uploader.total + ' Files';            			
			}
            
            this.uploading = false;
            
            if(e.target.upload.file.callback)
            	e.target.upload.file.callback(e.target.upload.file);
        },
        
        error: function(e){
            clearInterval(BlkLab.Uploader.timer);
        },

        abort: function(e){
            clearInterval(BlkLab.Uploader.timer);
        }  
	}

}(window, document));
