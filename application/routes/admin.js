var admin = require('../controllers/admin');
var express = require('express');
var router = express.Router();

router.route('/').get(admin.single);
router.route('/login').get(admin.login);
router.route('/logout').get(admin.logout);
router.route('/:id').get(admin.single);

module.exports = router;
