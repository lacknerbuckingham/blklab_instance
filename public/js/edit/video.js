BlkLab.VideoBlock = BlkLab.Block.extend({
	element: null,
	type: 'video',

	actions: {
		drop: function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			this.remove_class('entered');
			var scope = this;
			var files = ev.dataTransfer.files;
			if(files.length > 0){
				Videos.handle(files, scope);
			}
		},

		dragleave: function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			this.remove_class('entered');
		},

		dragenter: function(ev){
			ev.stopPropagation();
			ev.preventDefault();
		},

		dragover: function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			ev.dataTransfer.dropEffect = 'copy';
			this.remove_class('fade');
			this.add_class('entered');
		}
	},

	create: function(){
		this.element = Coffea.create('section', {'class':'blklab-block blklab-video'});
		this.element.data('type', 'Video');
	},
	
	edit: function(){
		var element = this.element;
		var target = this.element.children_by_tag('video').first();
		var ext = target.poster.split('.').pop();
		var base = target.poster.replace('http://', '').split('/')[0];
		var preview = target.poster;
		var data = {
			src: target.src,
			preview: preview
		};
		var template = BlkLab.templates['./public/js/templates/edit_video.hbs'];
		var html = template(data);

		var doc = $(document.documentElement);
		var wrapper = $('wrapper');		
		
		doc.add_class('noscroll');
		wrapper.add_class('disabled');
		
		var closeModal = function(){
			modal.remove_class('open');
			var timer = setInterval(function(){
				doc.remove_class('noscroll');
				wrapper.remove_class('disabled');				
				clearInterval(timer);
				timer = null;
				modal.destroy();
			},300);
		}

		var saveImage = function(){}

		var removeImage = function(){
			$(element).destroy();
			closeModal();
		}

		var modal = new Dialog('editor', html);
		modal.toggle_class('open');
		$('.cancel').click(closeModal);
		$('.submit').click(saveImage);
		$('.remove').click(removeImage);
	},

	render: function(container, insertionPoint, callback){
		var block = this.element;
		this.loadActions();
		if(!this.editing){
			var addImgButton = Coffea.create('div', {'class':'fa fa-film blklab-new-video'});
			var addImgForm = Coffea.create('form', {'method':'post'});
			var addImgInput = Coffea.create('input', {'type':'file', name:'blklab-videos'});
			addImgInput.multiple = false;
			addImgButton.append(addImgInput);
			addImgForm.append(addImgButton);
			block.append(addImgForm);
			if(insertionPoint){
				container.insertBefore(block, insertionPoint);
				insertionPoint = null;
			}else{
				container.appendChild(block);
			}

			addImgInput.change(function(ev){
				var scope = this;
				var files = ev.target.files;
				if(files.length > 0){
					Videos.handle(files, block);
				}
			});

			if(callback)
				callback.call(this);
		}
	}
});

/*this.videoBlock = function(ev){
			var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
			var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;
			var pattern3 = /([-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?(?:jpg|jpeg|gif|png))/gi;

			var html = '<h2><span class="fa fa-film"></span> Add Video</h2><div id="link_dialog"><input type="text" id="blklab-address" placeholder="Paste or type a link"><div class="clear"></div><input type="button" id="blklab-submit" value="Add"> <input type="button" id="blklab-cancel" value="Cancel"></div>';

			var modal = new Dialog('dialog', html, Evnt.target(ev), null, function(){
				var link = $('#blklab-address').value;
				var type = '';
				if(pattern1.test(link)){
					console.log('vimeo');
					var replacement = '<iframe src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
					var html = link.replace(pattern1, replacement);
					type = 'vimeo';
				}


				if(pattern2.test(link)){
					console.log('youtube');
					var replacement = '<iframe src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';
					var html = link.replace(pattern2, replacement);
					type = 'youtube';
				}


				if(pattern3.test(link)){
					console.log('img');
					var replacement = '<a href="$1" target="_blank"><img class="sml" src="$1" /></a><br />';
					var html = link.replace(pattern3, replacement);
					type = 'img';
				}

				if(type){
					var block = Coffea.create('section', {'class':'blklab-block blklab-video widescreen ' + type});
					block.data('type', 'video');
					block.innerHTML = html;
					if(_self.point){
						_self.content.insertBefore(block, _self.point);
						_self.point = null;
					}else{
						_self.content.appendChild(block);
					}
					modal.destroy();
				}else{
					alert('invalid video url');
				}
			});

			$('#blklab-address').value = 'http://vimeo.com/9214773';

			_self.sort.update();
		}*/
