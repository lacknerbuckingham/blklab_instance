var model = require('../models/pages');
var moment = require('moment');
var sec = require('../../library/security');
var extend = require('util')._extend;

var controllers = {
    all: function(req, res) {
        model.findAll().then(function(data){
			res.send(data);
		});
    },

    add: function(req, res) {
        sec.check(req).then(
			function(){
				var d = moment().unix();
                var params = {
                    props: {
                        title: req.body.title,
                        url: req.body.url,
                        created: d,
                        lastUpdated: d
                    }
                };

                model.createPage(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );

    },

    single: function(req, res) {
        var params = {
            url: req.params.id
        };

		model.findByUrl(params).then(function(data){
			sec.check(req).then(function(){
				var ret = extend({},data[0].page.data);
				ret['logged_in'] = true;
				data[0].page.data = ret;
				res.send(data);
			},function(){
				res.send(data);
			});
		}, function(err){
			res.status(404);
			res.send({error: 'page not found'});
		});
    },

    update: function(req, res) {
        sec.check(req).then(
            function(){
                var props = req.body;
                props.lastUpdated =  moment().unix();

                var params = {
                    url: req.params.id,
                    props: props
                };
                model.updatePage(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

    del: function(req, res) {
        sec.check(req).then(
            function(){
                var params = {
                    url: req.params.id
                };
                model.deletePage(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
    },

	getLinks: function(req, res){
		var params = {
            url: req.params.id
        };
		model.findLinkedPages(params).then(function(data){
			res.send(data);
		});
	},

	link: function(req, res){
        sec.check(req).then(
            function(){
                var params = {
                    url: req.params.id,
                    url2: req.body.url
                };
                model.linkPages(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

	deleteLink: function(req, res){
        sec.check(req).then(
            function(){
                var params = {
                    url: req.params.id,
                    url2: req.body.url
                };
                model.deletePageLink(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

	addNavLink: function(req, res){
        sec.check(req).then(
            function(){
                var params = {
                    url: req.body.url,
                    props: {
                        isnav: true,
                        nav_order:1
                    }
                };
                model.updatePage(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

	deleteNavLink: function(req, res){
        sec.check(req).then(
            function(){
                var params = {
                    url: req.params.id,
                    props: {
                        isnav: false,
                        nav_order:0
                    }
                };
                model.updatePage(params).then(function(data){
                    res.send(data);
                });
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

	updateMainNavSort: function(req, res){
        sec.check(req).then(
            function(){
                var payload = req.body;
                for(var key in payload){
                    var params = {
                        url: key,
                        props: {
                            nav_order: payload[key]
                        }
                    };
                    model.updatePage(params).then(function(data){});
                }
                res.send({message: 'Sort Updated'});
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	}

};

module.exports = controllers
