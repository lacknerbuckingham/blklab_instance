var Model = require('blklab').Neo4jModel;
var util = require('util');

var adminModel = Model.extend({});

var model = new adminModel({
	db:'lb',
	collection: 'admin'
});

model.schema({});

module.exports = model;
