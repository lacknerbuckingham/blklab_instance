BlkLab.View = function(){}

BlkLab.View.prototype = {
	template: '',

	init: function(){},

	model: {},

	render: function(el){
		var self = this;
		var template = typeof this.template === 'string' ? Handlebars.compile(this.template) : this.template;
		var data = this.model.data ? this.model.data : this.model;
		var html;

		if(data.length >= 1){			
			var d = {data: data};
			html = template(d);
		}else if (data.length == undefined){
			html = template({data: [data]});
		}else{
			html = template({});
		}

		var ele = typeof el == Object ? el : document.querySelector(el);
		ele.innerHTML = html;
	}
}

BlkLab.View.extend = function(methods){
	var self =  function(){
		BlkLab.View.call(this);
	}

	self.prototype = Object.create(BlkLab.View.prototype);
	self.prototype.constructor = self;

	for(method in methods){
		if(self.prototype.hasOwnProperty(method) === false) {
			self.prototype[method] = methods[method];
		}
	}
	return self;
}
