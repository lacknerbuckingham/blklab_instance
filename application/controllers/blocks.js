var Model = require('blklab').Model;
var blocksModel = require('../models/blocks');

var controllers = {
	all: function(req, res) {
		blocksModel.load({"deleted":{$ne:1}}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	add: function(req, res) {
		Model.insert("blocks", req.body, function(err, result){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(result);
			}
		});
	},

	single: function(req, res) {
		blocksModel.findOne({"identifier": req.params.id, "deleted":{$ne:1}}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	update: function(req, res) {
		blocksModel.update({"identifier": req.params.id}, req.body, function(err, result){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send({"result": result});
			}
		});
	},

	del: function(req, res) {
		blocksModel.update({"identifier": req.params.id}, {"deleted": 1}, function(err, result){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send({"result": result});
			}
		});
	},

};

module.exports = controllers
