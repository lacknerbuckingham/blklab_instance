var Model = require('blklab').Model;
var util = require('util');

var blocksModel = Model.extend({});

var model = new blocksModel({
	db:'lb',
	collection: 'blocks'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
