var uuid = require('node-uuid');
var model = require('../models/users');
var moment = require('moment');
var bcrypt = require('bcrypt');
var extend = require('util')._extend;
var sec = require('../../library/security');
var sess;

function removePassword(ret){
	var n = {}
	for(var key in ret){
		if(key != 'password' && key != 'token'){
			n[key] = ret[key];
		}
	}
	return n;
}

var controllers = {
    all: function(req, res) {
        model.findAll().then(function(data){
			var ret;
			var len = data.length;
			var i;
			for(i=0;i<len;i++){
				var tmp = //removePassword(data[i].users.data);
				ret = extend({},data[i].users.data);
				delete ret['password'];
				delete ret['token'];
				data[i].users.data = ret;
				//ret.push(tmp);
			}
			res.send(data);
		});
    },

    add: function(req, res) {
		var params = {
            username: req.body.username
        };

		model.findByUsername(params).then(function(data){
			if(data.length == 0){
				bcrypt.genSalt(10, function(err, salt) {
					bcrypt.hash(req.body.password, salt, function(err, hash) {
						sess = req.session;
						sess.email = req.body.username;
						sess.token = uuid.v1();
						var params = {
							props: {
								username: req.body.username,
								password: hash,
								created: moment(),
								lastUpdated: moment(),
								token: sess.token
							}
						};

						model.createUser(params).then(function(data){
							var tmp = JSON.parse(data);
							var ret = removePassword(tmp[0].user.data) || [];
							res.send(ret);
						});
					});
				});
			}else{
				res.send({error: 'Please Choose another username.'});
			}
		});
    },

	verify: function(req, res){
        sec.check(req).then(
            function(){
                res.send({msg: 'Authorized'});
            },
            function(){
                res.status(401);
				res.send({error: 'Unauthorized'});
            }
        );
	},

	authenticate: function(req, res){
		if( req.body.username &&  req.body.password){
			var params = {
				username: req.body.username
			};

			model.findByUsername(params).then(function(data){
				if(data.length > 0){
					var hash = data[0].user.data.password;
					var password = req.body.password;
					bcrypt.compare(password, hash, function(err, resp) {
						if(resp){
							sess = req.session;
							sess.email = req.body.username;
							sess.token = uuid.v1();
							var params = {
								username: req.body.username,
								props: {token: sess.token}
							};
							var ret = removePassword(data[0].user.data);
							model.updateUser(params).then(function(data){
								if(data.length > 0){
									res.send(ret);
								}else{
									res.send([]);
								}
							});
						}else{
							res.status(401);
							res.send({error: 'Unauthorized'});
						}
					});
				}else{
					res.status(401);
					res.send({error: 'Unauthorized'});
				}
			});
		}else{
			res.status(401);
			res.send({error: 'Unauthorized'});
		}
	},

	logout: function(req, res){
		sess = req.session;
		sess.email = '';
		sess.token = '';
		res.send({msg: 'Logged out'});
	},

    single: function(req, res) {
        var params = {
            username: req.params.id
        };

		model.findByUsername(params).then(function(data){
			if(data.length > 0){
				var ret = removePassword(data[0].user.data);
				res.send(ret);
			}else{
				res.send([]);
			}
		});
    },

    update: function(req, res) {
		var props = req.body;
		props.lastUpdated =  moment();

        var params = {
            username: req.params.id,
            props: props
        };
		model.updateUser(params).then(function(data){
			res.send(data);
		});
	},

    del: function(req, res) {
        var params = {
            username: req.params.id
        };
		model.deleteUser(params).then(function(data){
			res.send(data);
		});
	}

};

module.exports = controllers
