var Model = require('blklab').Model;
var util = require('util');

var TemplatesModel = Model.extend({});

var model = new TemplatesModel({
	db:'lb',
	collection: 'templates'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
