BlkLab.App.MediaModel = BlkLab.Model.extend();
BlkLab.App.MediaModel.url = '/api/media';	

BlkLab.App.MediaView = BlkLab.View.extend({
	template: this.templates['./public/js/templates/media_library.hbs']
});

BlkLab.App.MediaListView = BlkLab.View.extend({
	template: this.templates['./public/js/templates/media_library_list.hbs']
});


var dir = 'none';

BlkLab.Block = function(edit){
	if(!edit){
		this.editing = false;
		this.element;
		this.create();
	}else{
		this.editing = true;
		this.element = edit;
	}
	
	this.loadDefaultActions();
}

BlkLab.Block.prototype = {
	insertionPoint: '',
	
	actions:{},

	render: function(){
		this.loadActions();
	},

	loadActions: function(element){
		element = element ? element : this.element;
		if(element){
			var key;
			for(key in this.actions){
				element.on(key, this.actions[key]);
			}
		}
	},
	
	loadDefaultActions: function(){
		var self = this;
		var timeout;
		var held = false;
		this.element.on('dblclick', function(e){
			e.preventDefault();
			e.stopPropagation();
			var range    = document.createRange();
			var sel      = window.getSelection();
			sel.removeAllRanges();
			self.edit(e);
		});
		
		this.element.drag(this.drag);	
		this.element.dragstart(this.dragstart);			
		this.element.dragover(this.over);
		this.element.dragenter(this.enter);				
		this.element.dragleave(this.leave);				
		this.element.drop(this.drop);
		this.element.dragend(this.dragend);
	},
	
	createInsertionPoint: function(){
		var uuid = this.element.data('uuid');
		var previous = $(this.element.previousSibling);
		var puuid = '';
		if(previous){
			puuid = previous.data('uuid');
		}
		
		if(puuid != uuid || !uuid){
			var point = Coffea.create('div', {'class': 'blklab-insertion-point'});
			point.innerHTML = '+ Add New Block Here...';
			this.element.parentNode.insertBefore(point, this.element);
			point.add_class('on');			
			this.insertionPoint = point;
			return point;
		}
	},
	
	removeInsertionPoint: function(){		
		this.insertionPoint.destroy();
	},
	
	enable: function(){
		this.element.draggable = true;
		var point = this.createInsertionPoint();	
		return point;
	},
	
	disable: function(){
		this.element.draggable = false;
		if(this.insertionPoint)
			this.removeInsertionPoint();	
	},
	
	dragstart: function(ev){
		var target = Evnt.target(ev);
		ev.dataTransfer.effectAllowed = 'move';
		ev.dataTransfer.setData('text/html', target.innerHTML);		
		BlkLab.BlockUtils.movingElement = target;
		ev.dataTransfer.setDragImage(target, 50, 50);
		target.add_class('moving');	
	},
	
	drag: function(ev){
		ev.preventDefault();
		ev.stopPropagation();		
	},
	
	drop: function(ev){
		ev.preventDefault();
		ev.stopPropagation();		
		var target = BlkLab.BlockUtils.receivingElement;
		target.remove_class('entered');
		BlkLab.BlockUtils.reorder(ev);
		
		
		BlkLab.BlockUtils.clearZones();
		BlkLab.BlockUtils.movingElement.remove_class('moving');			
		BlkLab.BlockUtils.movingElement = null;
		BlkLab.BlockUtils.currentZone = '';
	},
	
	over: function(ev){
		ev.stopPropagation();
		ev.preventDefault();
		var target = Evnt.target(ev);
		var zone;
		var cls;
		BlkLab.BlockUtils.receivingElement = target;
		
		if(BlkLab.BlockUtils.movingElement){
			var element = document.elementFromPoint(ev.clientX, ev.clientY);
			if(element != BlkLab.BlockUtils.movingElement){
				element = $(element);
				while(element && !element.has_class('blklab-block')){
					element = $(element.parentNode);	
				}
				if(element && BlkLab.BlockUtils.receivingElement){
					zone = BlkLab.BlockUtils.checkZone(element, ev);
					if(BlkLab.BlockUtils.currentZone != dir){
						cls = 'blklab-' + zone;
						if(!element.has_class(cls)){
							BlkLab.BlockUtils.clearZones();
							element.add_class(cls);
						}
					}	
					dir = zone;													
				}
			}
		}
	},
	
	enter: function(ev){
		ev.preventDefault();
		ev.stopPropagation();		
		dir = 'none';
		BlkLab.BlockUtils.clearAllZones();
		var element = Evnt.target(ev);
		element = $(element);
		while(element && !element.has_class('blklab-block')){
			element = $(element.parentNode);	
		}
		if(element && element != BlkLab.BlockUtils.movingElement){
			BlkLab.BlockUtils.receivingElement = element;	
		}
	},
	
	leave: function(ev){
		ev.preventDefault();
		ev.stopPropagation();		
		BlkLab.BlockUtils.clearAllZones();
		BlkLab.BlockUtils.receivingElement = null;
	},
	
	dragend: function(ev){
		var target = Evnt.target(ev);
		BlkLab.BlockUtils.clearZones();		
		if(BlkLab.BlockUtils.receivingElement){		
			BlkLab.BlockUtils.receivingElement.remove_class('entered');							
			BlkLab.BlockUtils.receivingElement = null;
		}
		if(BlkLab.BlockUtils.movingElement){		
			BlkLab.BlockUtils.movingElement.remove_class('moving');							
			BlkLab.BlockUtils.movingElement = null;
			BlkLab.BlockUtils.currentZone = '';		
		}
		BlkLab.BlockUtils.clearAllZones();
	},
	
	edit: function(e){
		console.log('editing');
	},

	create: function(){
		console.log('test');
	}
}

BlkLab.Block.extend = function(methods){
	var self = function(arguments){
		BlkLab.Block.call(this, arguments);
	}

	self.prototype = Object.create(BlkLab.Block.prototype);
	self.prototype.constructor = self;

	for(method in methods){
		if(self.prototype.hasOwnProperty(method) === false) {
			self.prototype[method] = methods[method];
		}
	}

	return self;
}


var win = $(window);

BlkLab.BlockUtils = {
	currentZone: '',
	lastZone:'',
	movingElement:'',
	receivingElement:'',
	
	zones: ['blklab-left', 'blklab-right', 'blklab-above', 'blklab-below'],
	
	checkZone: function(element, ev){
		if(this.isLeft(element, ev)){
			this.currentZone = 'left';	
			return 'left';
		}else if(this.isMiddle(element, ev)){
			if(this.isAbove(element, ev)){
				this.currentZone = 'above';
				return 'above';				
			}else{
				this.currentZone = 'below';	
				return 'below';				
			}
		}else if(this.isRight(element, ev)){
			this.currentZone = 'right';	
			return 'right';			
		}
	},
	
	clearZones: function(global, ele){
		var receiving;
		if(!global){
			receiving = BlkLab.BlockUtils.receivingElement;
			while(receiving && !receiving.has_class('blklab-block')){
				receiving = receiving.parent;
			}
		}else{
			receiving = ele;
		}

		if(receiving){
			var i;
			var zones = this.zones;
			var len = zones.length; 
			for(i=0;i<len;i++){
				receiving.remove_class(zones[i]);
			}
		}
	},
	
	clearAllZones: function(){
		var self = BlkLab.BlockUtils;
		$('.blklab-block').each(function(el){
			self.clearZones(true, el);
		});	
	},
	
	reorder: function(ev){
		var receiving = BlkLab.BlockUtils.receivingElement;
		while(receiving && !receiving.has_class('blklab-block')){
			receiving = receiving.parent;
		}
		
		var zone = BlkLab.BlockUtils.currentZone;
		var moving = BlkLab.BlockUtils.movingElement;		
		var tmp = moving;
		var parent = receiving.parentNode;
		moving.destroy()		
		if(zone == 'above' || zone == 'left'){
			parent.insertBefore(tmp, receiving);
		}else{
			if(receiving.nextSibling != null){
				parent.insertBefore(tmp, receiving.nextSibling);
			}else{
				parent.append(tmp);
			}
		}
		
		if(zone == 'left' || zone == 'right'){
			this.formatAsCol(moving, receiving);
		}
		
		if(zone == 'above' || zone == 'below'){
			this.formatAsRow(moving);
		}
		
		this.cleanupFormat();
	},
	
	formatAsRow: function(element){
		var uuid = element.data('uuid');
		element.remove_data('uuid');
		element.preg_remove_class('span_.*?_of_.*?');		
		if(uuid){
			var pairs = $('[data-uuid="' + uuid + '"]');
			var cnt = pairs.length;
			var cls = 'span_1_of_' + cnt;
			if(cnt > 1){
				pairs.each(function(el){
					el.preg_remove_class('span_.*?_of_.*?');
					el.add_class(cls);			
				});	
			}else{
				var ele = pairs.first();
				ele.remove_data('uuid');
				ele.preg_remove_class('span_.*?_of_.*?');		
			}
		}
	},
	
	formatAsCol: function(adding, receiving){
		var uuid = receiving.data('uuid');
		if(uuid){
			adding.data('uuid', uuid);
			var pairs = $('[data-uuid="' + uuid + '"]');
			var cnt = pairs.length;
			var cls = 'span_1_of_' + cnt;
			pairs.each(function(el){
				el.preg_remove_class('span_.*?_of_.*?');
				el.add_class(cls);
				el.data('uuid', uuid);			
			});	
		}else{
			uuid = Coffea.Utils.generateUUID();
			var cls = 'span_1_of_2';
			adding.add_class(cls);
			receiving.add_class(cls);
			adding.data('uuid', uuid);
			receiving.data('uuid', uuid);			
		}
	},
	
	cleanupFormat: function(){
		var struct = {};
		var pairs = $('[data-uuid]');
		var uuid;
		pairs.each(function(el){
			var uuid = el.data('uuid');
			if(!struct[uuid]){
				struct[uuid] = [];
			}
			struct[uuid].push(el);
		});
		
		var key;
		var i;
		var el;
		var cls;
		for(key in struct){
			var group = struct[key];
			if(group.length > 1){
				cls = 'span_1_of_' + group.length;
				group.forEach(function(ele, i){
					el = $(ele);
					el.preg_remove_class('span_.*?_of_.*?');
					el.add_class(cls);
				});
			}else{
				el = group[0];
				el.remove_data('uuid');
				el.preg_remove_class('span_.*?_of_.*?');				
			}
		}
	},
	
	isMiddle: function(element, ev){
		var bounds = element.bounds();
		var widthPercent = bounds.w * 0.33333333;
		var xboundary1 = widthPercent;
		var xboundary2 = widthPercent * 2;
		var xboundary3 = widthPercent * 3;
		var relativeX = ev.clientX - bounds.x;
		return relativeX > xboundary1 && relativeX <= xboundary2
	},

	isAbove: function(element, ev){
		var bounds = element.bounds();
		var heightPercent = bounds.h * 0.5;
		var yboundary1 = heightPercent;
		var relativeY = ev.clientY - bounds.y;
		return relativeY < yboundary1;
	},

	isLeft: function(element, ev){
		var bounds = element.bounds();
		var widthPercent = bounds.w * 0.33333333;
		var xboundary1 = widthPercent;
		var xboundary2 = widthPercent * 2;
		var xboundary3 = widthPercent * 3;
		var relativeX = ev.clientX - bounds.x;
		return relativeX <= xboundary1;
	},

	isRight: function(element, ev){
		var bounds = element.bounds();
		var widthPercent = bounds.w * 0.33333333;
		var xboundary1 = widthPercent;
		var xboundary2 = widthPercent * 2;
		var xboundary3 = widthPercent * 3;
		var relativeX = ev.clientX - bounds.x;

		return relativeX > xboundary2;
	}
}