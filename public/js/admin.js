(function() {
	BlkLab.App.PagesModel = BlkLab.Model.extend();
    BlkLab.App.PagesModel.url = '/api/pages';
	
	BlkLab.App.PagesLinksModel = BlkLab.Model.extend();
    BlkLab.App.PagesLinksModel.url = '/api/pages/links';	

	BlkLab.App.UsersModel = BlkLab.Model.extend();
    BlkLab.App.UsersModel.url = '/api/users';
	
	BlkLab.App.MediaModel = BlkLab.Model.extend();
    BlkLab.App.MediaModel.url = '/api/media';	

    //DASHBOARD
    BlkLab.App.DashboardView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/dashboard.hbs']
    });


	BlkLab.App.DashboardController = BlkLab.Controller.extend({
        actions: {},

        render: function(route) {
			$('[data-href="/admin"]').first().parent.add_class('selected');											
            var view = new BlkLab.App.DashboardView();
			view.model = {};
			view.render('#inner');
			BlkLab.App.StackController.refreshActions();
		}
    });


	//STACK
	BlkLab.App.StackView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/stack.hbs']
    });

	BlkLab.App.MainNavView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/main_nav.hbs']
    });
	
	BlkLab.App.SubNavView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/sub_nav.hbs']
    });	

	BlkLab.App.StackController = BlkLab.Controller.extend({
		currentCollection:'home',
		subNav:null,
		dragElement:null,
		tmpElement:null,
		mainNav: null,
		mainLinks: null,
		subLinks: null,	
		path: null,			
		x: 0,
		y: 0,		

        actions: {
			drag_page: function(e){
				BlkLab.App.StackController.dragElement = Evnt.target(e);
			},

			drop_page: function(e){
				e.preventDefault();
				var target = Evnt.target(e);
				target.remove_class('entered');								
				var ele = BlkLab.App.StackController.dragElement;

				if(!ele.has_class('main_link') && !ele.parent.has_class('main_link')){
					var url = ele.data('url');
					var title = ele.data('title');

					BlkLab.App.StackController.mainLinks = $('.main_link');
					var test = BlkLab.App.StackController.mainLinks.filter(function(ele){
						return ele.getAttribute('data-url') == url;
					});

					if(test.length == 0){
						var li = BlkLab.create('li', {'draggable': 'true', 'class': 'main_link'});
						li.innerHTML = title;
						li.data('url', url);
						var tools = BlkLab.create('div', {'class': 'tools'});
						var remove_page = BlkLab.create('span', {'class': 'fa fa-times bad click'});
						remove_page.data('url', url);
						remove_page.click(BlkLab.App.StackController.actions.delete_link);
						tools.append(remove_page);

						li.drag(BlkLab.App.StackController.actions.sort_link);
						li.drop(BlkLab.App.StackController.actions.drop_link);
						li.dragover(BlkLab.App.StackController.actions.link_over);
						li.dragenter(BlkLab.App.StackController.actions.link_enter);

						li.append(tools);
						if($(target).has_class('main_link')){
							BlkLab.App.StackController.mainNav.insertBefore(li, target);
						}else{
							BlkLab.App.StackController.mainNav.append(li);
						}

						var payload = {
							url: url,
							main_nav: 1
						}
						post({
							url: '/api/pages/nav',
							data: JSON.stringify(payload)
						}).then(function(http){
							console.log(http.response);
						});

					}else{
						console.log('Add a message dialog function to show error');
					}
				}
				BlkLab.App.StackController.dragElement = null;
			},

			page_enter: function(e){
				e.preventDefault();
				var target = Evnt.target(e);
				target.add_class('entered');
			},

			page_over: function(e){
				e.preventDefault();
			},
			
			page_leave: function(e){
				var target = Evnt.target(e);
				target.remove_class('entered');													
			},

			delete_link: function(e){
				var parent = $(this.parentNode.parentNode);
				parent.destroy();
				del({
					url: '/api/pages/nav/' + this.getAttribute('data-url')
				}).then(function(http){
					console.log(http.response);
				});
			},

			sort_link: function(e){
				var target = Evnt.target(e);
				if(target.has_class('main_link') || target.parent.has_class('main_link')){
					BlkLab.App.StackController.dragElement = target;
					if(BlkLab.App.StackController.x == 0)
						BlkLab.App.StackController.x = e.clientX;
					target.add_class('moving');
				}else if(target.has_class('sub_link') || target.parent.has_class('sub_link')){
					BlkLab.App.StackController.dragElement = target;
					if(BlkLab.App.StackController.y == 0)
						BlkLab.App.StackController.y = e.clientY;
					target.add_class('moving');
				}
			},

			drop_link: function(e){
				var target = Evnt.target(e);
				var ele = BlkLab.App.StackController.dragElement;
				if(!ele.has_class('page')){
					var dir = 'right';
					if(e.clientX < BlkLab.App.StackController.x){
						dir = 'left';
					}
					BlkLab.App.StackController.x = 0;
					if(dir == 'right'){
						if(target.nextSibling){
							BlkLab.App.StackController.mainNav.insertBefore(ele, target.nextSibling)
						}else{
							BlkLab.App.StackController.mainNav.append(ele);
						}
					}else{
						try{
							BlkLab.App.StackController.mainNav.insertBefore(ele, target)
						}catch(e){
							ele.remove_class('moving');
						}
					}
					ele.remove_class('moving');

					var payload = {};
					var i = 0;
					BlkLab.App.StackController.mainLinks = $('.main_link');
					BlkLab.App.StackController.mainLinks.each(function(link){
						i++;
						payload[link.data('url')] = i;
					});

					put({
						url: '/api/pages/nav/sort',
						data: JSON.stringify(payload)
					}).then(function(http){
						console.log(http.response);
					});



				}
			},

			link_enter: function(e){
				e.preventDefault();

			},

			link_over: function(e){
				e.preventDefault();
			},

			new_page: function(e){
				var uid = BlkLab.Utils.generateUUID();
				var list = $('page-list');
				var new_li = BlkLab.create('li', {});
				var new_input = BlkLab.create('input', {type: 'text', placeholder: 'New Page Name', autofocus:'true'});

				var accept = BlkLab.create('span', {'class': 'fa fa-check good click'});
				var decline = BlkLab.create('span', {'class': 'fa fa-times bad click'});

				new_input.data('id', uid);
				accept.data('id', uid);
				accept.click(function(e){
					var slug = new_input.value.toSlug();
					BlkLab.App.PagesModel.setData({
						title: new_input.value,
						url: new_input.value.toSlug()
					})
					BlkLab.App.PagesModel.save(true).then(function(resp){
						new_input.destroy();
						accept.destroy();
						decline.destroy();
						new_li.id = "page-" + slug;
						new_li.innerHTML = new_input.value;
						var tools = BlkLab.create('div', {'class': 'tools'});
						var remove_page = BlkLab.create('span', {'class': 'fa fa-times bad click'});
						remove_page.setAttribute('blklab-click', 'delete_page');
						remove_page.data('url', slug);
						remove_page.click(BlkLab.App.StackController.actions.delete_page);
						tools.append(remove_page);
						new_li.append(tools);
					});
				});

				decline.data('id', uid);
				decline.click(function(e){
					new_li.destroy();
				});

				new_li.append(new_input);
				new_li.append(accept);
				new_li.append(decline);
				list.append(new_li);
				new_li.focus();
			},

			search_pages: function(e){
				var target = $('search_pages');
				var data = BlkLab.dataStore['/api/pages'];
				var results;
				if(target.value != ''){
					results = BlkLab.Search(data, function(item){
						if(item.title.indexOf(target.value) != -1){
							return item;
						}
					});
				}else{
					results = data;
				}
				BlkLab.App.PagesModel.setData(results);

				var view = new BlkLab.App.StackView();
				view.model = BlkLab.App.PagesModel;
                view.render('#inner');
                BlkLab.App.StackController.refreshActions();
			},

			delete_page: function(e){
				var target = Evnt.target(e);
				target.hide();
				var li = $('page-' + target.data('url'));

				var accept = BlkLab.create('span', {'class': 'fa fa-check good click'});
				var decline = BlkLab.create('span', {'class': 'fa fa-times bad click'});

				var confirm = function(){
					BlkLab.App.PagesModel.del(target.data('url')).then(function(resp){
						li.destroy();
					});
				}

				var deny = function(){
					accept.destroy();
					decline.destroy();
					target.show(true);
				}

				accept.click(confirm);
				decline.click(deny);

				target.parentNode.appendChild(accept);
				target.parentNode.appendChild(decline);
			},
			
			drop_subpage: function(e){
				e.preventDefault();
				var target = Evnt.target(e);
				target.remove_class('entered');
				
				var ele = BlkLab.App.StackController.dragElement;
				if(!ele.has_class('sub_link')){
					var url = ele.data('url');
					var title = ele.data('title');
					var li = BlkLab.create('li', {'draggable': 'true', 'class': 'sub_link'});
					li.innerHTML = title;
					li.data('url', url);
					var tools = BlkLab.create('div', {'class': 'tools'});
					var remove_page = BlkLab.create('span', {'class': 'fa fa-times bad click'});
					remove_page.data('url', url);
					remove_page.click(BlkLab.App.StackController.actions.delete_sub_link);
					tools.append(remove_page);

					li.drag(BlkLab.App.StackController.actions.sort_sub_link);
					li.drop(BlkLab.App.StackController.actions.drop_sub_link);
					li.dragover(BlkLab.App.StackController.actions.link_over);
					li.dragenter(BlkLab.App.StackController.actions.link_enter);
					li.append(tools);

					BlkLab.App.StackController.subNav.append(li);

					var payload = {
						url: url
					}
					post({
						url: '/api/pages/links/' + BlkLab.App.StackController.currentCollection,
						data: JSON.stringify(payload)
					}).then(function(http){
						console.log(http.response);
					});
				}
				BlkLab.App.StackController.dragElement = null;
			},
			
			drop_sub_link: function(e){
				var target = Evnt.target(e);
				var ele = BlkLab.App.StackController.dragElement;
				console.log(ele);
				console.log(target);
				ele.remove_class('moving');				
				if(!ele.has_class('page')){
					var dir = 'down';
					if(e.clientY < BlkLab.App.StackController.y){
						dir = 'up';
					}
					BlkLab.App.StackController.y = 0;
					if(dir == 'down'){
						if(target.nextSibling){
							BlkLab.App.StackController.subNav.insertBefore(ele, target.nextSibling)
						}else{
							BlkLab.App.StackController.subNav.append(ele);
						}
					}else{
						try{
							BlkLab.App.StackController.subNav.insertBefore(ele, target)
						}catch(e){
							ele.remove_class('moving');
						}
					}
					ele.remove_class('moving');

					var payload = {};
					var i = 0;
					BlkLab.App.StackController.subLinks = $('.sub_link');
					BlkLab.App.StackController.subLinks.each(function(link){
						i++;
						payload[link.data('url')] = i;
					});
					
					console.log(payload);
					/*put({
						url: '/api/pages/nav/sort',
						data: JSON.stringify(payload)
					}).then(function(http){
						console.log(http.response);
					});*/
				}
			},
			
			delete_sub_link: function(e){
				var parent = $(this.parentNode.parentNode);
				parent.destroy();
				var payload = {
					url: this.getAttribute('data-url')
				}
				del({
					url: '/api/pages/links/' + BlkLab.App.StackController.currentCollection,
					data: JSON.stringify(payload)
				}).then(function(http){
					console.log(http.response);
				});
			},
			
			show_collection: function(ev){
				var target = Evnt.target(ev);
				var url = target.data('url');
				var title = target.data('title');				
				BlkLab.App.StackController.currentCollection = url;
				
				BlkLab.App.PagesLinksModel.find({'id': url}).then(function(model) {
					BlkLab.App.StackController.path.innerHTML = title;
					BlkLab.App.StackController.subNav.innerHTML = '';
					var view = new BlkLab.App.SubNavView();
					view.model = model;
					view.render('#sub_nav');
					BlkLab.App.StackController.refreshActions();
				});
			}
		},

        render: function(route) {
			var self = this;
			this.route = route;
			BlkLab.App.PagesModel.find().then(function(model) {
				$('[data-href="/admin/stack"]').first().parent.add_class('selected');								
				var view = new BlkLab.App.StackView();
				view.model = model;
                view.render('#inner');
				BlkLab.App.StackController.mainNav = $('#main_nav');
				BlkLab.App.StackController.subNav = $('#sub_nav');	
				BlkLab.App.StackController.path = $('#path');					

				var navView = new BlkLab.App.MainNavView();
				var d = model.data.filter(function(item){
					if(item.isnav){
						return item;
					}
				});
				d = d.sort(function(a, b) {
					a = a.nav_order;
					b = b.nav_order;

					return a < b ? -1 : (a > b ? 1 : 0);
				});
				//BlkLab.App.PagesModel.setData(d);
				navView.model = d;
                navView.render('#main_nav');

				window.onkeyup = function(e){
					var target = Evnt.target(e);
					if(e.which === 13 && target.getAttribute('data-type') == 'search'){
						BlkLab.App.StackController.actions.search_pages(e);
					}
				}

                BlkLab.App.StackController.refreshActions();
            });
		}
    });


	//USERS
	BlkLab.App.UsersView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/users.hbs']
    });

	BlkLab.App.UsersController = BlkLab.Controller.extend({
        actions: {
			new_user: function(e){
				var uid = BlkLab.Utils.generateUUID();
				var list = $('user-list');
				var new_li = BlkLab.create('li', {});
				var new_input = BlkLab.create('input', {type: 'text', placeholder: 'New Username', autofocus:'true', 'class':'fifty'});
				var new_password = BlkLab.create('input', {type: 'text', placeholder: 'New Password', 'class':'fifty'});

				var accept = BlkLab.create('span', {'class': 'fa fa-check good click'});
				var decline = BlkLab.create('span', {'class': 'fa fa-times bad click'});

				new_input.data('id', uid);
				accept.data('id', uid);
				accept.click(function(e){
					BlkLab.App.UsersModel.setData({
						username: new_input.value
					})
					/*BlkLab.App.UsersModel.save().then(function(resp){
						new_input.destroy();
						accept.destroy();
						decline.destroy();
						new_li.id = "page-" + slug;
						new_li.innerHTML = new_input.value;
						var tools = BlkLab.create('div', {'class': 'tools'});
						var remove_page = BlkLab.create('span', {'class': 'fa fa-times bad click'});
						remove_page.setAttribute('blklab-click', 'delete_page');
						remove_page.data('url', slug);
						remove_page.click(BlkLab.App.StackController.actions.delete_page);
						tools.append(remove_page);
						new_li.append(tools);
					});*/
				});

				decline.data('id', uid);
				decline.click(function(e){
					new_li.destroy();
				});

				new_li.append(new_input);
				new_li.append(new_password);
				new_li.append(accept);
				new_li.append(decline);
				list.append(new_li);
				new_li.focus();
			},

			search_users: function(e){

			},

			delete_user: function(e){

			}
		},

        render: function(route) {
			var self = this;
			BlkLab.App.UsersModel.find().then(function(model) {
				$('[data-href="/admin/users"]').first().parent.add_class('selected');				
				var view = new BlkLab.App.UsersView();
				view.model = model;
                view.render('#inner');
                BlkLab.App.UsersController.refreshActions();
            });
		}
    });


	//CONFIG
	BlkLab.App.ConfigView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/config.hbs']
    });

	BlkLab.App.ConfigController = BlkLab.Controller.extend({
        actions: {},

        render: function(route) {
			$('[data-href="/admin/config"]').first().parent.add_class('selected');								
			var view = new BlkLab.App.ConfigView();
			view.model = {};
			view.render('#inner');
			BlkLab.App.ConfigController.refreshActions();
		}
    });

	//Media
	BlkLab.App.MediaView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/media.hbs']
    });
	
	BlkLab.App.MediaRowView = BlkLab.View.extend({
        template: this.templates['./public/js/templates/media_row.hbs']
    });
	
	var files;
	var files_list;	
	var tmp;
	var selected_files;
	
	BlkLab.App.MediaController = BlkLab.Controller.extend({
		loadFiles: function(files){
			if(files.length > 0){
				var len = files.length;
				var i;
				for(i=0;i<len;i++){
					var file = files[i];
					BlkLab.Uploader.add_to_queue(file, function(e){
						var row = BlkLab.create('li', {id: 'row_' + e.name});
						var tools = BlkLab.create('div', {'class': 'tools'});
						var del = BlkLab.create('span', {'class': "fa fa-times bad", 'blklab-click': "delete_image", 'data-url': e.name});	
						del.parent = tools;
						row.innerHTML = e.name;
						tools.append(del);
						row.append(tools);
						files_list.append(row);
						BlkLab.App.MediaController.refreshActions();
					});
				}

				BlkLab.Uploader.start();
			}
		},
		
        actions: {
			drop_image: function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				files.remove_class('entered');
				var file_list = ev.dataTransfer.files;
				BlkLab.App.MediaController.loadFiles(file_list);
			},
			
			select_image: function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				var file_list = ev.target.files;
				BlkLab.App.MediaController.loadFiles(file_list);	
			},

			list_enter: function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				files.add_class('entered');
			},

			list_over: function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				ev.dataTransfer.dropEffect = 'copy';
			},
			
			list_leave: function(ev){
				ev.preventDefault();
				files.remove_class('entered');
			},
			
			delete_image: function(e){
				var target = Evnt.target(e);
				var name = target.data('url');
				var tmp = target.parentNode.innerHTML;
				
				function confirm(){
					var row = $('[data-url="' + name + '"]').first();
					
					del({
						url: '/api/media/' + name
					}).then(function(http){
						console.log(http.response);
					});
					
					row.add_class('disappear');
					var t = window.setInterval(function(){
						window.clearInterval(t);
						row.destroy();
					}, 3000);
				}
				
				function deny(){
					target.innerHTML = tmp;
					BlkLab.App.MediaController.refreshActions();
				}
				
				target.parentNode.innerHTML = '';
				
				var good = BlkLab.create('span', {'class': "fa fa-check good click"});											
				var bad = BlkLab.create('span', {'class': "fa fa-times bad click"});											
				
				good.click(confirm);
				bad.click(deny);
				
				target.parent.append(good);
				target.parent.append(bad);
				
			},
			
			select: function(e){
				var target = Evnt.target(e);
				var name = target.id;					
				if(!target.has_class('selected')){
					target.add_class('selected');
					selected_files.push(name);
				}else{
					var i = selected_files.indexOf(name);
					target.remove_class('selected');
					delete selected_files[i];					
				}
			},
			
			delete_selected: function(){
				var files = selected_files;
				var len = selected_files.length;
				var row;
				var i = 0;
				
				var timer = window.setInterval(function(){
					if(i < len){
						var row = $(files[i]);
						i++;
						if(row){
							var name = row.data('url');

							del({
								url: '/api/media/' + name
							}).then(function(http){
								console.log(http.response);
							});

							row.add_class('disappear');
							var t = window.setInterval(function(){
								window.clearInterval(t);
								row.destroy();
							}, 3000);	
						}
					}else{
						window.clearInterval(timer);	
					}
				}, 600);
			}
		},

        render: function(route) {
			var self = this;
			selected_files = [];
			this.route = route;
			BlkLab.App.MediaModel.find().then(function(model) {
				$('[data-href="/admin/media"]').first().parent.add_class('selected');
				var view = new BlkLab.App.MediaView();
				view.model = model;
				view.render('#inner');
				files = $('files');
				files_list = $('file-list');				
				BlkLab.App.MediaController.refreshActions();
				BlkLab.App.lazyLoad(files_list);
			});
		}
    });
	
	//Messages
	BlkLab.App.MessagesView = BlkLab.View.extend({
		template: this.templates['./public/js/templates/messages.hbs']
    });

	BlkLab.App.MessagesController = BlkLab.Controller.extend({
        actions: {},

        render: function(route) {
			$('[data-href="/admin/messages"]').first().parent.add_class('selected');
			var self = this;
			var view = new BlkLab.App.MessagesView();
			view.model = {};
			view.render('#inner');
			BlkLab.App.MessagesController.refreshActions();
		}
    });


	//Routes
    BlkLab.App.Router.routes({
		default: {
            controller: BlkLab.App.DashboardController
        },

		'/admin/stack': {
            controller: BlkLab.App.StackController
        },

		'/admin/users': {
            controller: BlkLab.App.UsersController
        },

		'/admin/config': {
            controller: BlkLab.App.ConfigController
        },

		'/admin/media': {
            controller: BlkLab.App.MediaController
        },

		'/admin/messages': {
            controller: BlkLab.App.MessagesController
        },

    });
	
	var links;
	BlkLab.History.start();
	BlkLab.History.callback = function(href){
		links = $('.selected')
		links.remove_class('selected');
		$(this.parentNode).add_class('selected');
	}
    BlkLab.App.run();
	
	
	
})();
