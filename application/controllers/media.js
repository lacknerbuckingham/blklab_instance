var model = require('../models/media');
var fs = require('fs-extra');
var uuid = require('node-uuid');
var img = require('../../library/image.js');
var sys = require('sys');
var exec = require('child_process').exec;

// Link two pages together
// MATCH (website:Website {domain:"dev"})<-[:OWNS]->(page), (website:Website {domain:"dev"})<-[:OWNS]->(page2) WHERE page.url="/about" AND page2.url="/home" CREATE (page)-[:LINKSTO]->(page2) RETURN page, page2

var controllers = {
	all: function(req, res){
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + host + '"})<-[:HASIMAGE]->(images) RETURN images'
		model.load(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	add: function(req, res) {
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];

		var mimes = {
			"image/jpeg": '.jpg',
			"image/png": '.png',
			"image/gif": '.gif'
		};

		var config = require('../../config');
		var fstream;
		req.pipe(req.busboy);
		req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
			var base_name = uuid.v1();
			var ext = mimes[mimetype];

			var large = base_name + '-lg' + ext;
			var medium = base_name + '-md' + ext;
			var small = base_name + '-sm' + ext;
			var thumb = base_name + '-thumb' + ext;
			var original = base_name + ext;
			var path = config.uploads + filename;
			var output_path = config.static + '/assets/images/';

			fstream = fs.createWriteStream(path);
			file.pipe(fstream);
			fstream.on('close', function () {
				var gm = require('gm').subClass({imageMagick: true});
				var orig = gm(path);

				orig.resize(2000, 2000, "^>").autoOrient().noProfile().quality(80).write(output_path + large, function (err) {
					if(!err){

					}else{
						console.log(err);
					}
				});

				orig.resize(1200, 1200, "^>").autoOrient().noProfile().quality(80).write(output_path + medium, function (err) {
					if(!err){
					}else{
						console.log(err);
					}
				});

				orig.resize(640, 640, "^>").autoOrient().noProfile().quality(80).write(output_path + small, function (err) {
					if(!err){
					}else{
						console.log(err);
					}
				});

				orig.gravity('Center').resize(400, 400, "^>").crop(400, 400).autoOrient().noProfile().quality(80).write(output_path + thumb, function (err) {
					if(!err){
					}else{
					}
				});
				console.log(req.body);
				var query = 'MATCH (website:Website {domain:"' + host + '"}) CREATE (image:Image {title:"' + req.body.title + '", src:"' + original + '", basename:"' + base_name + '", caption:"' + req.body.caption + '"}) CREATE (website)-[:HASIMAGE]->(image)'
				model.insert(query, {}, function(err, result){
					if(err){
						res.send('Error ' + err);
					}else{
						res.redirect('back');
					}
				});

			});
		});
	},

	single: function(req, res) {
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + host + '"})<-[:HASIMAGE]->(image) WHERE image.original="' + req.params.id + '" RETURN image'
		model.findOne(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	update: function(req, res){
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];

		var update = []
		for(var key in req.body){
			update.push(' page.' + key + '="' + req.body[key] + '"');
		}
		update = update.join(', ');

		var query = 'MATCH (website:Website {domain:"' + host + '"})<-[:HASIMAGE]->(image) WHERE image.original="' + req.params.id + '" SET ' + update + ' RETURN image'
		model.update(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	del: function(req, res) {
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + host + '"})<-[rel:HASIMAGE]->(image) WHERE image.original="' + req.params.id + '" DELETE rel, image';
		model.del(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

};

module.exports = controllers
