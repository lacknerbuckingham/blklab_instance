var model = require('../models/sites');
var pkgcloud = require('pkgcloud');

var client = pkgcloud.storage.createClient({
  provider: 'rackspace',
  username: 'bairdb',
  apiKey: 'cf7f82229fba584820efd5d050b7067b',
  region: 'ORD'
});

var controllers = {
	all: function(req, res){
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + host + '"}) RETURN website'
		model.load(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	add: function(req, res) {
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];

		var createSite = function(uri){
			var query = 'CREATE (website:Website {title:"' + req.body.title + '", domain: "' + req.body.domain + '", uri:"' + uri + '"})'
			model.insert(query, {}, function(err, result){
				if(err){
					res.send('Error ' + err);
				}else{
					res.send(result);
				}
			});
		}

		var uri;
		client.getContainer(req.body.domain, function(err, container) {
			if(err){
				client.createContainer({name: req.body.domain}, function (err, container) {
					container.enableCdn(function(err){
						client.getContainer(req.body.domain, function(err, container) {
							uri = container.cdnUri;
							createSite(uri);
						});
					});
				});
			}else{
				uri = container.cdnUri;
				createSite(uri);
			}
		});
	},

	single: function(req, res) {
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + host + '"}) RETURN website'
		model.load(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send(items);
			}
		});
	},

	del: function(req, res){
		var host = req.headers.referer.replace('http://','').replace('www','').split('/')[0];
		var query = 'MATCH (website:Website {domain:"' + req.params.id + '"}) OPTIONAL MATCH (website)-[r]-() DELETE website, r'
		model.load(query, {}, function(err, items){
			if(err){
				res.send('Error ' + err);
			}else{
				res.send('Cleared');
			}
		});
	}

};

module.exports = controllers
