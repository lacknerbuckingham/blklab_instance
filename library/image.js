var fs = require('fs-extra');
var uuid = require('node-uuid');

var BLImage = function(filename, mimetype){
	_self = this;
	this.config = require('../config');
	this.mimes = {
		"image/jpeg": '.jpg',
		"image/png": '.png',
		"image/gif": '.gif'
	};
	this.gm = require('gm').subClass({imageMagick: true});
	this.basename;
	this.ext;
	this.mimetype = mimetype;

	this.orig;

	this.init = function(){
		this.basename = uuid.v1();
		this.ext = this.mimes[this.mimetype];
		//this.orig = gm(output_path + original);
	}

	this.resize = function(w, h, postfix){
		this.orig.resize(w, h, "^>").autoOrient().noProfile().quality(80).write(output_path + postfix, function (err) {
			if(!err){

			}else{
				console.log(err);
			}
		});
	}

	this.init();
}

exports.BLImage = BLImage;
